<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Handle an incoming request.
     * Define the role allowed the the User Access Token
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string[]  ...$guards
     * @return mixed
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards)
    {
        // Authenticate the user
        $this->authenticate($request, $guards);

        // Get the Authenticated user
        $user = \Auth::user();
        if ($user) {
            $as = null;
            // Check that he has the role for the scope requested
            if ($user->tokenCan('regular')) {
                $as = 'regular';
            } elseif ($user->tokenCan('owner')) {
                $as = 'owner';
            } elseif ($user->tokenCan('admin')) {
                $as = 'admin';
            }
            // If the Role exist, we set the property to the Authenticated user
            if ($as && $user->roles()->where('name', $as)->count() > 0) {
                $user->setAs($as);
            }
        }
        
        return $next($request);
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('404');
        }
    }
}
