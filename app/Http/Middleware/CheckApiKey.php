<?php

namespace App\Http\Middleware;

use Closure;

class CheckApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('api_key') != env('API_KEY')) {
            return \LinckoJson::error(401, 'You are not authorized to access this API.');
        }
        return $next($request);
    }
}
