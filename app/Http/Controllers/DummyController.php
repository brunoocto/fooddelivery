<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * DummyController is a controller for testing and debugging purpose only
 *
 */
class DummyController extends Controller
{
    /**
     * GET method test
     *
     * @return \Illuminate\Http\Response
     */
    public function getTest()
    {
        return 'getTest';
    }

    /**
     * POST method test
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postTest(Request $request)
    {
        $text = 'postTest';
        if ($request->has('test')) {
            $text = $request->input('test');
        }
        return $text;
    }
}
