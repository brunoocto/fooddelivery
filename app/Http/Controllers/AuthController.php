<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Users;
use App\Models\Roles;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController;
use App\Models\FoodDeliveryModel;

class AuthController extends Controller
{

    /**
     * Register a new user
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Laravel\Passport\Http\Controllers\AccessTokenController $controller
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request, AccessTokenController $controller)
    {
        // Check the inputs against the Model rules
        $rules = Users::getRulesList();

        // We request "as" here even if we give both scope "owner" and "regular", it helps to log in the application as an type of user
        Validator::make($request->all(), [
            'name' => $rules['name'],
            'email' => $rules['email'].'|unique:fooddelivery.users,email',
            'password' => $rules['password'],
            'password_confirmation' => $rules['password'].'|required_with:password|same:password',
            'as' => 'required|in:owner,regular' // We do not allow to create an admin account via API call
        ])->validate();

        // Use Transaction for user
        \DB::connection('fooddelivery')->beginTransaction();
        // Create the user
        $user = Users::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]);

        if (is_null($user)) {
            \DB::connection('fooddelivery')->rollBack();
            return \LinckoJson::error(400, 'An error occured, the User could not be created. Please try again.');
        }

        try {
            // Get Owner and Regular Roles (Default User registration)
            $roles = Roles::whereIn('name', ['owner', 'regular'])->get();
            $user->roles()->saveMany($roles);

            /**
             * Build the body according to Passport
             * https://laravel.com/docs/6.x/passport#requesting-password-grant-tokens
             */
            $body = [
                'grant_type' => 'password',
                'client_id' => env('PASSPORT_PASSWORD_GRANT_CLIENT_ID'),
                'client_secret' => env('PASSPORT_PASSWORD_GRANT_CLIENT_SECRET'),
                'username' => $request->input('email'), // We identify by unique email
                'password' => $request->input('password'),
                'scope' => $request->input('as'),
            ];

            // Update the Request instance with a new Body content to fit the need of the Passport controller called below
            FoodDeliveryModel::updateRequest($body);

            // Use Passport to generate Access and Refresh Token
            $new_request = app()->make('Psr\Http\Message\ServerRequestInterface');
            $response = $controller->issueToken($new_request);
            
            // Set Status to Successful Creation
            $response->setStatusCode(201);
        } catch (Exception $e) {
            \DB::connection('fooddelivery')->rollBack();
            return \LinckoJson::error(400, 'An error occured, the User could not be created. please try again.');
        }
        // Commit the User and Roles insertions
        \DB::connection('fooddelivery')->commit();

        return $response;
    }

    /**
     * Login an existing user
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Laravel\Passport\Http\Controllers\AccessTokenController $controller
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request, AccessTokenController $controller)
    {
        // Check the inputs against the Model rules
        $rules = Users::getRulesList();

        Validator::make($request->all(), [
            'email' => $rules['email'],
            'password' => $rules['password'],
            'as' => 'required|in:owner,regular,admin'
        ])->validate();

        // Find the user
        $user = Users::Where('email', $request['email'])->first();
        if (is_null($user)) {
            return \LinckoJson::error(422, 'Wrong Email or Password.');
        }

        // Get the role
        $as = $request->input('as');
        // Check if the user has access to the role
        $role = $user->roles()->where('roles.name', $as)->first();
        if (is_null($role)) {
            return \LinckoJson::error(422, 'You do not have the following role: '.$as);
        }

        /**
         * Build the body according to Passport
         * https://laravel.com/docs/6.x/passport#requesting-password-grant-tokens
         */
        $body = [
            'grant_type' => 'password',
            'client_id' => env('PASSPORT_PASSWORD_GRANT_CLIENT_ID'),
            'client_secret' => env('PASSPORT_PASSWORD_GRANT_CLIENT_SECRET'),
            'username' => $request->input('email'), // We identify by unique email
            'password' => $request->input('password'),
            'scope' => $as,
        ];

        // Update the Request instance with a new Body content to fit the need of the Passport controller called below
        FoodDeliveryModel::updateRequest($body);

        // Use Passport to generate Access and Refresh Token
        $new_request = app()->make('Psr\Http\Message\ServerRequestInterface');
        $response = $controller->issueToken($new_request);

        return $response;
    }
    
    /**
     * Refresh an Access Token
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Laravel\Passport\Http\Controllers\AccessTokenController $controller
     * @return \Illuminate\Http\Response
     */
    public function refresh(Request $request, AccessTokenController $controller)
    {
        Validator::make($request->all(), [
            'refresh_token' => 'required',
        ])->validate();

        /**
         * Build the body according to Passport
         * https://laravel.com/docs/6.x/passport#refreshing-tokens
         */
        $body = [
            'grant_type' => 'refresh_token',
            'client_id' => env('PASSPORT_PASSWORD_GRANT_CLIENT_ID'),
            'client_secret' => env('PASSPORT_PASSWORD_GRANT_CLIENT_SECRET'),
            'refresh_token' => $request->input('refresh_token'),
        ];

        // Update the Request instance with a new Body content to fit the need of the Passport controller called below
        FoodDeliveryModel::updateRequest($body);

        // Use Passport to generate Access and Refresh Token
        $new_request = app()->make('Psr\Http\Message\ServerRequestInterface');
        $response = $controller->issueToken($new_request);

        return $response;
    }
  
    /**
     * Logout a User
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController $controller
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request, AuthorizedAccessTokenController $controller)
    {
        // Get the Authenticated User
        $user = \Auth::user();
        if (is_null($user)) {
            return \LinckoJson::error(400, 'No User found.');
        }

        // Get the Access Token
        $token = $user->token();
        if (is_null($token)) {
            return \LinckoJson::error(400, 'No Access Token found.');
        }

        // Use Passport to revoke the Access Token
        $response = $controller->destroy($request, $token->id);

        return $response;
    }
}
