<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Passport\Http\Controllers\AccessTokenController;

class UsersController extends Controller
{
    /**
     * Get the current log user information
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Laravel\Passport\Http\Controllers\AccessTokenController $controller
     * @return \Illuminate\Http\Response
     */
    public function me(Request $request, AccessTokenController $controller)
    {
        // Get the Authenticated User ID
        $user = \Auth::user();
        if (is_null($user)) {
            return \LinckoJson::error(400, 'No User found.');
        }
        // Add the scope
        $scope = [
            'as' => $user->getAs(),
        ];
        return \LinckoJson::send($user, 200, '', $scope);
    }
}
