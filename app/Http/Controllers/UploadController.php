<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Restaurants;
use App\Models\Meals;

//use GuzzleHttp\Psr7\Request as Guzzle;

class UploadController extends Controller
{
    public function upload(Request $request)
    {
        $item = null;
        if ($request->input('type') == 'restaurants') {
            $item = Restaurants::find($request->input('id'));
        } elseif ($request->input('type') == 'meals') {
            $item = Meals::find($request->input('id'));
        }
        
        if ($item) {
            foreach ($request->allFiles() as $file) {
                $path = $file->store('fooddelivery/'.$request->input('type'));
                $item->picture = $path;
                if ($item->save()) {
                    return $path;
                }
            }
        }
        return \LinckoJson::error(400, 'No User found.');
    }
}
