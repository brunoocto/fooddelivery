<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Users;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // We past the resource list to Vmodel resource logic
        $this->mergeConfigFrom(
            config_path('fooddelivery.resources.php'),
            'json-api-brunoocto-vmodel.resources'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Passport::tokensCan([
            'admin' => 'Administrator',
            'owner' => 'Restaurant owner',
            'regular' => 'Regular client',
        ]);
        // Link to the Users Class model specific to the API, the default is a virtual (non-persistant) one Brunoocto\Vmodel\Models\VmodelUsers
        config(['auth.providers.vmodel.model' => Users::class]);
    }
}
