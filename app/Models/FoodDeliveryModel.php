<?php

namespace App\Models;

use Brunoocto\Vmodel\Models\VmodelModel;
use Illuminate\Database\Eloquent\Builder;

/**
 * FoodDelivery model
 * An Abstract class for all models
 *
 */
class FoodDeliveryModel extends VmodelModel
{
    /**
     * Update the Request instance with a new Body content
     *
     * @param Array $body Body content
     * @return void
     */
    protected static function updateRequest(array $body)
    {
        // Modify the content property
        request()->request->replace($body);

        // Re-init the Request instance with the new Body content
        request()->initialize(
            request()->query->all(),
            request()->request->all(),
            request()->attributes->all(),
            request()->cookies->all(),
            request()->files->all(),
            request()->server->all(),
            json_encode($body, JSON_FORCE_OBJECT)
        );

        // Overrides the PHP global variables according to this request instance.
        request()->overrideGlobals();

        // Initialize some variables
        request()->getPathInfo();
        request()->getRequestUri();
        request()->getBaseUrl();
        request()->getBasePath();
        request()->getMethod();
        request()->getRequestFormat();
    }
    
    /**
     * Get the binary item CRUD for the current user
     *
     * @return string CRUD
     */
    public function getCRUD()
    {
        // Get Default CRUD
        $crud = parent::getCRUD();

        // Get Authenticated User to check his permission according to his roles
        $user = \Auth::user();

        if ($user) {
            $permissions = null;

            // An Authentocated user must have a Role
            if ($user->isAdmin()) {
                // We Give full access to the Administrator
                return '1111';
            } elseif (!is_null($user->getAs())) {
                // Get all Permissions to the user
                $permissions = Permissions::where('permissions.model', $this->getTable())
                    ->whereHas('roles', function (Builder $query) use ($user) {
                        $query->WhereHas('users', function ($query) use ($user) {
                            $query->where('users.id', $user->id)->where('name', $user->as);
                        });
                    })->get();
            }

            // If some permissions are setup, we overwrite the default behavior of Vmodel ($this->crud and crud_owner)
            if (!is_null($permissions) && $permissions->count() > 0) {
                // For each permission, we check all CRUD allowance and setup at 1 if at least one exists (like a OR operand)
                // For security reason (in case of typo issue), we default as 0
                $crud = '0000';
                foreach ($permissions as $permission) {
                    // Crud
                    if ($permission->create) {
                        $crud[0] = '1';
                    }
                    // cRud
                    if ($permission->read) {
                        $crud[1] = '1';
                    }
                    // crUd
                    if ($permission->update) {
                        $crud[2] = '1';
                    }
                    // cruD
                    if ($permission->delete) {
                        $crud[3] = '1';
                    }
                }
                // Return the evaluated CRUD level
                return $crud;
            }
        }

        // Return the default behavior
        return parent::getCRUD();
    }

    /**
     * Return the User-ID found via authentication middleware
     *
     * @return string
     */
    public function getAuthUserId()
    {
        $auth_id = \Auth::id();
        if (is_null($auth_id)) {
            // Return 0 as Admin ID if no User is authenticated. The Authentication should be checkd by a Middleware like 'api:auth' or 'auth' previously. It is not this class responsibility.
            return 0;
        }
        return $auth_id;
    }
}
