<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

use App\Models\FoodDeliveryModel;
use App\Models\Meals;
use App\Models\Restaurants;

/**
 * Orders model
 * List of Orders placed by a Regular User
 *
 * NOTE:
 * status change "status_history" are stored in the following format: [(int)timestamp, (int)status, (string)user_id]
 *
 */
class Orders extends FoodDeliveryModel
{
    // Add "deleted_at"
    use SoftDeletes;

    /**
    * All relationships
    *
    * @var array
    */
    protected static $model_relationships = [
        'restaurants' => [ // Display name
            true, // Authorize input
            true, // Visible
            'restaurants', // Method name
            'belongsTo', // Relation type
            Restaurants::class, // Class name
        ],
        'meals' => [
            true,
            true,
            'meals',
            'belongsToMany',
            Meals::class,
        ],
    ];

    /**
     * Storage validation rules
     *
     * @var array
     */
    protected static $rules = [
        'status' => 'numeric|between:1,6',
        'restaurants' => 'required',
    ];

    /**
     * Variable to tell if the object request has been modified (true)
     * in the contex of the class Orders only
     *
     * @var boolean
     */
    protected static $request_modified = false;

    /**
     * Database connection
     *
     * @var string
     */
    protected $connection = 'fooddelivery';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * List visible keys for json response.
     * Anything starting with a single underscore are reserved for relationships.
     * VmodelModel does always add by default the following fields if they exist in the table:
     *  'id',
     *  'created_at',
     *  'created_by',
     *  'updated_at',
     *  'updated_by',
     *  'deleted_at',
     *  'deleted_by',
     *
     * @var array
     */
    protected $visible = [
        'status',
        'status_history',
    ];

    /**
     * Force the output format of some keys
     *
     * @var array
     */
    protected $casts = [
        'status' => 'numeric',
        'status_history' => 'array',
    ];

    /**
     * Definition of Status Workflow
     * '{status number}' => [['{from which status}'], [{array of who can}]]
     * status 'null' means from creation
     * An Admin cannot change the status (as per requested)
     *
     * @var array
     */
    protected $status_workflow = [
        1 => [[null], ['regular']], // Placed
        2 => [[1], ['owner']], // Processing
        3 => [[2], ['owner']], // In Route
        4 => [[3], ['owner']], // Delivered
        5 => [[4], ['regular']], // Received
        6 => [[1, 2, 3, 4], ['regular']], // Canceled
    ];

    /**
     * Set that the request object has been already modified
     *
     * @return void
     */
    protected static function setRequestModied()
    {
        static::$request_modified = true;
    }

    /**
     * Add the Workflow check before saving
     *
     * @return boolean
     */
    protected function checkStatusChange()
    {
        // Check if it is defined
        $dirty = $this->getDirty();
        if (!isset($dirty['status'])) {
            // We do nothing
            return true;
        }

        $auth = \Auth::user();
        if (is_null($auth)) {
            // We authorize for seeding the DB
            return true;
        }

        if (isset($this->id)) {
            // Existing instance
            $status_old = intval($this->getOriginal('status'));
        } else {
            // New instance
            $status_old = null;
        }
        $status_new = intval($this->getAttribute('status'));
        
        if ($status_old == $status_new) {
            // We do nothing
            return true;
        }
        
        // We check that the Workflow status exists
        if (!isset($this->status_workflow[$status_new])) {
            \LinckoJson::error(405, 'This status does not exists.');
            return false;
        }

        // We check who can
        if (!in_array($auth->getAs(), $this->status_workflow[$status_new][1])) {
            if ($status_new==1) {
                \LinckoJson::error(405, 'You are not allowed to place an order.');
            } else {
                \LinckoJson::error(405, 'You are not allowed to update the status.');
            }
            return false;
        }

        // We check if we can move to the next status
        if (!in_array($status_old, $this->status_workflow[$status_new][0])) {
            \LinckoJson::error(405, 'This is not the next status.');
            return false;
        }

        // Record the change
        if (is_null($this->status_history)) {
            // We store the creation change
            $this->status_history =  [
                [now()->timestamp, $status_new, $auth->id],
            ];
        } else {
            // We add new change
            $status_history = $this->getAttribute('status_history');
            if (is_array($status_history)) {
                $this->status_history = array_merge($status_history, [
                    [now()->timestamp, $status_new, $auth->id],
                ]);
            } else {
                // In case the format has an issue, we store at least the latest change
                $this->status_history =  [
                    [now()->timestamp, $status_new, $auth->id],
                ];
            }
        }

        // The status can be changed
        return true;
    }

    /**
     * Unable any change of 'status_history' by the User
     *
     * @return boolean
     */
    protected function unableStatusHistoryChange()
    {
        // Check if it is defined
        $dirty = $this->getDirty();
        if (isset($dirty['status_history'])) {
            \LinckoJson::error(405, 'You are not allowed to modify the history.');
            return false;
        }

        return true;
    }


    /* -------- Start - Relationships -------- */

    /**
     * Get the Restaurants that own the Order.
     * One(Restaurants) to Many(Orders)
     *
     * @return App\Models\Restaurants
     */
    public function restaurants()
    {
        // 'restaurant_id' is a field used in Tasks class to define the relationship between both objects
        return $this->belongsTo(Restaurants::class, 'restaurant_id');
    }

    /**
     * Get all Meals that belong to the Order.
     * Many(Orders) to Many(Meals)
     *
     * @return App\Models\Meals::Collection
     */
    public function meals()
    {
        return $this->belongsToMany(Meals::class, 'order_x_meal', 'order_id', 'meal_id')->withPivot(['ordered_at', 'price', 'quantity']);
    }

    /* -------- End - Relationships -------- */
    
    /**
     * Constructor
     * For any Meals relationships, we make sure to use the correct pivot value from DB, we do not allow the user modify it
     *
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        // We avoid to request many times because the Class is intantiated multiple time
        if (!static::$request_modified) {
            $json = request()->json();
            $data = $json->get('data');

            // We check if we attach some Meals relationships
            if (isset($data['relationships']['meals']['data'])) {
                // For any Processing (alias "paid") or later order status, we do not allow to add anymore meals
                if (isset($this->id) && $this->status > 1) {
                    return \LinckoJson::error(405, 'Order under process, you are not allowed to modify the list of meals.');
                }
                // Get the restaurant
                $restaurant = null;
                if (isset($this->id)) {
                    $restaurant = $this->restaurants;
                } elseif (isset($data['relationships']['restaurants']['data']['id'])) {
                    $restaurant = Restaurants::find($data['relationships']['restaurants']['data']['id']);
                }
                if (!is_null($restaurant)) {
                    // Add the values
                    foreach ($data['relationships']['meals']['data'] as $key => $item) {
                        // If the pivot already exists, we don't modify it
                        if (!is_null($this->meals()->find($item['id']))) {
                            continue;
                        }
                        // If the meal does not belong to the restaurant
                        if (is_null($restaurant->meals()->find($item['id']))) {
                            continue;
                        }
                        if ($meal = Meals::find($item['id'])) {
                            // Overwrite the pivot values
                            $meta = [
                                'ordered_at' => now()->timestamp,
                                'price' => $meal->price,
                            ];
                            // Create "meta" array
                            if (!isset($data['relationships']['meals']['data'][$key]['meta'])) {
                                $data['relationships']['meals']['data'][$key]['meta'] = $meta;
                            } else {
                                // Merge the new array of Meals (quantity is kept if present)
                                $data['relationships']['meals']['data'][$key]['meta'] = array_merge($data['relationships']['meals']['data'][$key]['meta'], $meta);
                            }
                            // Default at 1
                            // Create "meta" array
                            if (!isset($data['relationships']['meals']['data'][$key]['meta']['quantity'])) {
                                $data['relationships']['meals']['data'][$key]['meta']['quantity'] = 1;
                            }
                        }
                    }
                }
                
                // Regenerate the Request Object
                FoodDeliveryModel::updateRequest(['data' => $data]);
                // Keep track that the Object request has been updated already
                static::setRequestModied();
            }
        }
    }

    /**
     * Add the Workflow check before saving
     *
     * @return boolean
     */
    public function save(array $options = [])
    {
        // For a new Order, reject it if the Regular user is blocked for from the corresponding restaurant
        if (!isset($this->id)) {
            // Get the current User
            $auth = \Auth::user();
            // If the user is authenticated as Regular
            if (!is_null($auth) && $auth->getAs() == 'regular') {
                $is_blocked = $this->restaurants->blockedUsers()->where('id', $auth->id)->where('user_x_restaurant.blocked', true)->first();
                // If we found a user instance
                if (!is_null($is_blocked)) {
                    // We reject the order creation
                    \LinckoJson::error(405, 'You are not allowed to place an order. Please contact the restaurant owner.');
                    return false;
                }
            }
        }
        
        // Check the history
        $this->unableStatusHistoryChange();
        // Check the status
        $this->checkStatusChange();
        
        return parent::save($options);
    }

    /**
     * Get with role conditions
     */
    public function newQuery()
    {
        $query = parent::newQuery();

        // Get the current User
        $auth = \Auth::user();
        // If the user is authentcated
        if (!is_null($auth)) {
            if ($auth->getAs() == 'owner') {
                // List all orders from all Owner's restaurants
                $query = $query->whereHas('restaurants', function (Builder $query) use ($auth) {
                    $query->where('created_by', $auth->id);
                });
            } elseif ($auth->getAs() == 'regular') {
                // List all orders created by the Regular user (including the ones from a restaurant blocked)
                /**
                 * Date: Nov 24, 2020
                 * Note: We Keep the Restaurants visible, even if the user is blocked
                 */
                // $query = $query->where('created_by', $auth->id)->whereHas('restaurants', function (Builder $query) use ($auth) {
                //     $query->whereDoesntHave('blockedUsers', function (Builder $query) use ($auth) {
                //         $query->where('user_x_restaurant.user_id', $auth->id)->where('user_x_restaurant.blocked', true);
                //     });
                // });
                $query = $query->where('created_by', $auth->id);
            }
        }

        return $query;
    }
}
