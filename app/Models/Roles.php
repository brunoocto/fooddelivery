<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\FoodDeliveryModel;
use App\Models\Users;
use App\Models\Permissions;

/**
 * Roles model
 * Role assigned to a User
 *
 */
class Roles extends FoodDeliveryModel
{
    // Add "deleted_at"
    use SoftDeletes;

    /**
    * All relationships
    *
    * @var array
    */
    protected static $model_relationships = [
        'permissions' => [ // Display name
            true, // Authorize input
            true, // Visible
            'permissions', // Method name
            'hasMany', // Relation type
            Permissions::class, // Class name
        ],
        'users' => [
            false,
            false,
            'users',
            'belongsToMany',
            Users::class,
        ],
    ];

    /**
     * Storage validation rules
     *
     * @var array
     */
    protected static $rules = [
        'name' => 'required|max:256',
    ];

    /**
     * Database connection
     *
     * @var string
     */
    protected $connection = 'fooddelivery';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * List visible keys for json response.
     * Anything starting with a single underscore are reserved for relationships.
     * VmodelModel does always add by default the following fields if they exist in the table:
     *  'id',
     *  'created_at',
     *  'created_by',
     *  'updated_at',
     *  'updated_by',
     *  'deleted_at',
     *  'deleted_by',
     *
     * @var array
     */
    protected $visible = [
        'name',
    ];

    /**
     * Force the output format of some keys
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
    ];


    /* -------- Start - Relationships -------- */

    /**
     * Get all Permissions that belong to the Role.
     * One(Roles) to Many(Permissions)
     *
     * @return App\Models\Permissions::Collection
     */
    public function permissions()
    {
        // 'role_id' is a field used in Permissions class to define the relationship between both objects
        return $this->hasMany(Permissions::class, 'role_id');
    }

    /**
     * Get all Users that belong to the Role.
     * Many(Roles) to Many(Users)
     *
     * @return App\Models\Users::Collection
     */
    public function users()
    {
        return $this->belongsToMany(Users::class, 'user_x_role', 'role_id', 'user_id');
    }

    /* -------- End - Relationships -------- */

    /**
     * Save the Model
     * Add some CRUD restriction according the Role
     *
     * @param array $options Additional parameters
     * @return boolean
     */
    public function save(array $options = [])
    {
        // For Authenticated user, we restrict CRUD operations
        $auth = \Auth::user();

        // If the Authenticated User is not an Admin
        if (!is_null($auth) && !is_null($auth->getAs()) && !$auth->isAdmin()) {
            // Reject Creation and Update
            \LinckoJson::error(405, 'Operation not allowed.');
            return false;
        }

        return parent::save($options);
    }
}
