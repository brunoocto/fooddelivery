<?php

namespace App\Models;

use App\Models\FoodDeliveryModel;
use App\Models\Roles;
use App\Models\Restaurants;

// Used for Authentocated User methods (from Illuminate\Foundation\Auth\User)
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;

// Needed for laravel/passport
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

/**
 * Users model
 *
 */
class Users extends FoodDeliveryModel implements
AuthenticatableContract,
AuthorizableContract,
CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail, HasApiTokens, Notifiable;

    /**
     * Act as a Role
     *
     * @var string
     */
    protected $as = null;

    /**
    * All relationships
    *
    * @var array
    */
    protected static $model_relationships = [
        'roles' => [ // Display name
            true, // Authorize input
            true, // Visible
            'roles', // Method name
            'belongsToMany', // Relation type
            Roles::class, // Class name
        ],
        'restaurants' => [
            true,
            true,
            'restaurants',
            'hasMany',
            Restaurants::class,
        ],
    ];

    /**
     * Storage validation rules
     *
     * @var array
     */
    protected static $rules = [
        'name' => 'required|max:256',
        'email' => 'required|max:190|email:rfc,dns',
        'password' => 'required|min:6|max:256',
    ];

    /**
     * Database connection
     *
     * @var string
     */
    protected $connection = 'fooddelivery';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * List visible keys for json response.
     * Anything starting with a single underscore are reserved for relationships.
     * VmodelModel does always add by default the following fields if they exist in the table:
     *  'id',
     *  'created_at',
     *  'created_by',
     *  'updated_at',
     *  'updated_by',
     *  'deleted_at',
     *  'deleted_by',
     *
     * @var array
     */
    protected $visible = [
        'email',
        'name',
    ];

    /**
     * Force the output format of some keys
     *
     * @var array
     */
    protected $casts = [
        'email' => 'string',
        'name' => 'string',
        'email_verified_at' => 'datetime',
    ];

    /**
     * Default CRUD restriction per model class
     * 0100:  R
     * 1100: CR
     * 1110: CRU
     * 1111: CRUD (default)
     *
     *  @var array
     */
    protected $crud = '1100';

    /**
     * Default CRUD restriction per model class for the owner
     * 0100:  R
     * 1100: CR
     * 1110: CRU
     * 1111: CRUD (default)
     *
     *  @var array
     */
    protected $crud_owner = '1110';

    /* -------- Start - Relationships -------- */

    /**
     * Get all Roles that belong to the User.
     * Many(Users) to Many(Roles)
     *
     * @return App\Models\Roles::Collection
     */
    public function roles()
    {
        return $this->belongsToMany(Roles::class, 'user_x_role', 'user_id', 'role_id');
    }

    /**
     * Get all Restaurants that belong to the User.
     * One(Users) to Many(Restaurants)
     *
     * @return App\Models\Restaurants::Collection
     */
    public function restaurants()
    {
        // 'created_by' is a field used in Restaurants class to define the relationship between both objects
        return $this->hasMany(Restaurants::class, 'created_by');
    }
    
    /**
     * Save the Model
     * Add some CRUD restriction according the Role
     *
     * @param array $options Additional parameters
     * @return boolean
     */
    public function save(array $options = [])
    {
        // For Authenticated user, we restrict CRUD operations
        $auth = \Auth::user();

        // If the Authenticated User is not an Admin
        if (!is_null($auth) && !is_null($auth->getAs()) && !$auth->isAdmin()) {
            // Reject Creation, and Update of any other User
            if (!isset($this->id) || $this->id != $auth->id) {
                \LinckoJson::error(405, 'Operation not allowed.');
                return false;
            }
        }

        return parent::save($options);
    }

    /**
     * Get the binary item CRUD for the current user
     *
     * @return string CRUD
     */
    public function getCRUD()
    {
        // Get User ID
        $user_id = $this->getAuthUserId();

        // For the user itself, created_by is replaced by id, so we set $crud same as $crud_owner
        if ($this->id == $user_id) {
            $this->crud = $this->crud_owner;
        }

        return parent::getCRUD();
    }

    /**
     * Check if the authenticated user has the scope Admin active
     *
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->as == 'admin';
    }

    /**
     * Disable Deletion
     *
     * @return false
     */
    public function delete()
    {
        \LinckoJson::error(405, 'Operation not allowed.');
        return false;
    }

    /**
     * Disable Restoration
     *
     * @return false
     */
    public function restoreItem()
    {
        \LinckoJson::error(405, 'Operation not allowed.');
        return false;
    }

    /**
     * $as Setter
     *
     * @param string $role
     * @return void
     */
    public function setAs(string $role)
    {
        $this->as = $role;
    }

    /**
     * $as Getter
     *
     * @return string
     */
    public function getAs()
    {
        return $this->as;
    }
}
