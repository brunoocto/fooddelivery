<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\FoodDeliveryModel;
use App\Models\Roles;

/**
 * Permissions model
 * CRUD Permissions given to a Role
 *
 */
class Permissions extends FoodDeliveryModel
{
    // Add "deleted_at"
    use SoftDeletes;

    /**
    * All relationships
    *
    * @var array
    */
    protected static $model_relationships = [
        'roles' => [ // Display name
            true, // Authorize input
            true, // Visible
            'roles', // Method name
            'belongsTo', // Relation type
            Roles::class, // Class name
        ],
    ];

    /**
     * Storage validation rules
     *
     * @var array
     */
    protected static $rules = [
        'model' => 'required|max:256|in:users,roles,permissons,restaurants,meals,orders',
        'roles' => 'required',
    ];

    /**
     * Database connection
     *
     * @var string
     */
    protected $connection = 'fooddelivery';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * List visible keys for json response.
     * Anything starting with a single underscore are reserved for relationships.
     * VmodelModel does always add by default the following fields if they exist in the table:
     *  'id',
     *  'created_at',
     *  'created_by',
     *  'updated_at',
     *  'updated_by',
     *  'deleted_at',
     *  'deleted_by',
     *
     * @var array
     */
    protected $visible = [
        'model',
        'create',
        'read',
        'update',
        'delete',
    ];

    /**
     * Force the output format of some keys
     *
     * @var array
     */
    protected $casts = [
        'model' => 'string',
        'create' => 'boolean',
        'read' => 'boolean',
        'update' => 'boolean',
        'delete' => 'boolean',
    ];


    /* -------- Start - Relationships -------- */

    /**
     * Get the Roles that own the Tasks.
     * One(Roles) to Many(Permissions)
     *
     * @return App\Models\Roles
     */
    public function roles()
    {
        // 'role_id' is a field used in Tasks class to define the relationship between both objects
        return $this->belongsTo(Roles::class, 'role_id');
    }

    /* -------- End - Relationships -------- */

    /**
     * Save the Model
     * Add some CRUD restriction according the Role
     *
     * @param array $options Additional parameters
     * @return boolean
     */
    public function save(array $options = [])
    {
        // For Authenticated user, we restrict CRUD operations
        $auth = \Auth::user();

        // If the Authenticated User is not an Admin
        if (!is_null($auth) && !is_null($auth->getAs()) && !$auth->isAdmin()) {
            // Reject Creation and Update
            \LinckoJson::error(405, 'Operation not allowed.');
            return false;
        }

        return parent::save($options);
    }
}
