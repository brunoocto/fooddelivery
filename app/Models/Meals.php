<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

use App\Models\FoodDeliveryModel;
use App\Models\Restaurants;
use App\Models\Orders;

/**
 * Meals model
 *
 */
class Meals extends FoodDeliveryModel
{
    // Add "deleted_at"
    use SoftDeletes;

    /**
    * All relationships
    *
    * @var array
    */
    protected static $model_relationships = [
        'restaurants' => [ // Display name
            true, // Authorize input
            true, // Visible
            'restaurants', // Method name
            'belongsTo', // Relation type
            Restaurants::class, // Class name
        ],
        'orders' => [
            false,
            false,
            'orders',
            'belongsToMany',
            Orders::class,
        ],
    ];

    /**
     * Storage validation rules
     *
     * @var array
     */
    protected static $rules = [
        'name' => 'required|max:256',
        'description' => 'required',
        'price' => 'required|numeric|min:0',
        'restaurants' => 'required',
    ];

    /**
     * Database connection
     *
     * @var string
     */
    protected $connection = 'fooddelivery';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'meals';

    /**
     * List visible keys for json response.
     * Anything starting with a single underscore are reserved for relationships.
     * VmodelModel does always add by default the following fields if they exist in the table:
     *  'id',
     *  'created_at',
     *  'created_by',
     *  'updated_at',
     *  'updated_by',
     *  'deleted_at',
     *  'deleted_by',
     *
     * @var array
     */
    protected $visible = [
        'name',
        'description',
        'price',
        'picture',
    ];

    /**
     * Force the output format of some keys
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'price' => 'float',
        'picture' => 'string',
    ];


    /* -------- Start - Relationships -------- */

    /**
     * Get the Restaurants that own the Meal.
     * One(Restaurants) to Many(Meals)
     *
     * @return App\Models\Restaurants
     */
    public function restaurants()
    {
        // 'restaurant_id' is a field used in Tasks class to define the relationship between both objects
        return $this->belongsTo(Restaurants::class, 'restaurant_id');
    }

    /**
     * Get all Orders that belong to the Meal.
     * Many(Meals) to Many(Orders)
     *
     * @return App\Models\Orders::Collection
     */
    public function orders()
    {
        return $this->belongsToMany(Orders::class, 'order_x_meal', 'meal_id', 'order_id')->withPivot(['ordered_at', 'price', 'quantity']);
    }

    /* -------- End - Relationships -------- */

    /**
     * Save the Model
     * Add some CRUD restriction according the Role
     *
     * @param array $options Additional parameters
     * @return boolean
     */
    public function save(array $options = [])
    {
        // For Authenticated user, we restrict CRUD operations
        $auth = \Auth::user();

        // If the Authenticated User is a Regular user
        if (!is_null($auth) && $auth->getAs() == 'regular') {
            // Reject Creation and Update
            \LinckoJson::error(405, 'Operation not allowed.');
            return false;
        }

        return parent::save($options);
    }

    /**
     * Get with role conditions
     */
    public function newQuery()
    {
        $query = parent::newQuery();
        
        // Get the current User
        $auth = \Auth::user();
        // If the user is authentcated
        if (!is_null($auth)) {
            if ($auth->getAs() == 'owner') {
                // List all meals from all Owner's restaurants
                $query = $query->whereHas('restaurants', function (Builder $query) use ($auth) {
                    $query->where('created_by', $auth->id);
                });
            } elseif ($auth->getAs() == 'regular') {
                // List all meals visible by the Regular user
                /**
                 * Date: Nov 24, 2020
                 * Note: We Keep the Restaurants visible, even if the user is blocked
                 */
                // $query = $query->whereHas('restaurants', function (Builder $query) use ($auth) {
                //     $query->whereDoesntHave('blockedUsers', function (Builder $query) use ($auth) {
                //         $query->where('user_x_restaurant.user_id', $auth->id)->where('user_x_restaurant.blocked', true);
                //     });
                // });
            }
        }

        return $query;
    }
}
