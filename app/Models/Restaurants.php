<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

use App\Models\FoodDeliveryModel;
use App\Models\Meals;
use App\Models\Orders;

/**
 * Restaurants model
 *
 */
class Restaurants extends FoodDeliveryModel
{
    // Add "deleted_at"
    use SoftDeletes;

    /**
    * All relationships
    *
    * @var array
    */
    protected static $model_relationships = [
        'meals' => [ // Display name
            true, // Authorize input
            true, // Visible
            'meals', // Method name
            'hasMany', // Relation type
            Meals::class, // Class name
        ],
        'orders' => [
            true,
            true,
            'orders',
            'hasMany',
            Orders::class,
        ],
        'blocked_users' => [
            true,
            true,
            'blockedUsers',
            'belongsToMany',
            Users::class,
        ],
    ];

    /**
     * Storage validation rules
     *
     * @var array
     */
    protected static $rules = [
        'name' => 'required|max:256',
        'description' => 'required',
    ];

    /**
     * Database connection
     *
     * @var string
     */
    protected $connection = 'fooddelivery';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'restaurants';

    /**
     * List visible keys for json response.
     * Anything starting with a single underscore are reserved for relationships.
     * VmodelModel does always add by default the following fields if they exist in the table:
     *  'id',
     *  'created_at',
     *  'created_by',
     *  'updated_at',
     *  'updated_by',
     *  'deleted_at',
     *  'deleted_by',
     *
     * @var array
     */
    protected $visible = [
        'name',
        'description',
        'picture',
    ];

    /**
     * Force the output format of some keys
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'picture' => 'string',
    ];


    /* -------- Start - Relationships -------- */

    /**
     * Get all Meals that belong to the Restaurant.
     * One(Restaurants) to Many(Meals)
     *
     * @return App\Models\Meals::Collection
     */
    public function meals()
    {
        // 'restaurant_id' is a field used in Meals class to define the relationship between both objects
        return $this->hasMany(Meals::class, 'restaurant_id');
    }

    /**
     * Get all Orders that belong to the Restaurant.
     * One(Restaurants) to Many(Orders)
     *
     * @return App\Models\Orders::Collection
     */
    public function orders()
    {
        // 'restaurant_id' is a field used in Orders class to define the relationship between both objects
        return $this->hasMany(Orders::class, 'restaurant_id');
    }

    /**
     * Get all "blocked" Users that belong to the Restaurant.
     * Many(Restaurants) to Many(Users)
     *
     * @return App\Models\Users::Collection
     */
    public function blockedUsers()
    {
        return $this->belongsToMany(Users::class, 'user_x_restaurant', 'restaurant_id', 'user_id')->withPivot(['blocked'])->where('user_x_restaurant.blocked', true);
    }

    /* -------- End - Relationships -------- */

    /**
     * Save the Model
     * Add some CRUD restriction according the Role
     *
     * @param array $options Additional parameters
     * @return boolean
     */
    public function save(array $options = [])
    {
        // For Authenticated user, we restrict CRUD operations
        $auth = \Auth::user();

        // If the Authenticated User is a Regular user
        if (!is_null($auth) && $auth->getAs() == 'regular') {
            // Reject Creation and Update
            \LinckoJson::error(405, 'Operation not allowed.');
            return false;
        }

        return parent::save($options);
    }

    /**
     * Get with role conditions
     */
    public function newQuery()
    {
        $query = parent::newQuery();

        // Get the current User
        $auth = \Auth::user();
        // If the user is authentcated
        if (!is_null($auth)) {
            if ($auth->getAs() == 'owner') {
                // List all Owner's restaurants
                $query = $query->where('created_by', $auth->id);
            } elseif ($auth->getAs() == 'regular') {
                // List all unblocked restaurants
                // NOTE: whereDoesntHave takes all restaurants by default if no pivot specified

                /**
                 * Date: Nov 24, 2020
                 * Note: We Keep the Restaurants visible, even if the user is blocked
                 */
                // $query = $query->whereDoesntHave('blockedUsers', function (Builder $query) use ($auth) {
                //     $query->where('user_x_restaurant.user_id', $auth->id)->where('user_x_restaurant.blocked', true);
                // });
            }
        }

        return $query;
    }
}
