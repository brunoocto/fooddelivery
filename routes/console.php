<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

// Complete refresh of all DB
\Artisan::command('fooddelivery:fresh', function () {
    // We create files if they do not exists yet (testing is using memory, so the file won't exists)
    if (env('APP_ENV') != 'testing') {
        if (config('database.connections.sqlite.driver') == 'sqlite' && !file_exists(config('database.connections.sqlite.database'))) {
            touch(config('database.connections.sqlite.database'));
        }
        if (config('database.connections.fooddelivery.driver') == 'sqlite' && !file_exists(config('database.connections.fooddelivery.database'))) {
            touch(config('database.connections.fooddelivery.database'));
        }
    }
    // We don't need to Wide the default DB because "migrate:fresh" do it
    print_r("[-  ] Processing\n");
    \Artisan::call('db:wipe', ['--database' => 'fooddelivery']);
    print_r(Artisan::output());
    print_r("\n[-- ] Processing\n");
    \Artisan::call('migrate:fresh', ['--seed' => true]);
    print_r(Artisan::output());
    print_r("\n[---] All DBs are refreshed\n");
})->describe('Complete refresh of all DB');
