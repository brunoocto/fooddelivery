<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Map VND resources routes
 * Make sure that namespace and folder architecture respect PSR-4 standard.
 *
 */
\Route::group([
    'middleware' => ['api','auth:api'],
    'prefix' => 'fooddelivery',
], function () {
    \LinckoVmodel::mapConfigResources(config_path('fooddelivery.resources.php'));
});

/**
 * Customs routes
 *
 */
\Route::group([
    'middleware' => ['api','auth:api'],
    'prefix' => 'fooddelivery',
], function () {
    // Get Authenticated user instance (with no relationships)
    \Route::get('/users/me', 'UsersController@me')->name('fooddelivery.users.me');
});

// Files
\Route::post('/files/upload', 'UploadController@upload')
    ->middleware('auth:api')
    ->name('files.upload');
    
/**
 * Credential routes
 *
 */
\Route::group([
    'middleware' => ['api',],
    'prefix' => 'auth',
], function () {
    
    // Public routes
    \Route::post('/register', 'AuthController@register')->name('auth.register');
    \Route::post('/login', 'AuthController@login')->name('auth.login');

    // Secured routes
    \Route::group([
        'middleware' => ['auth:api',],
    ], function () {
        \Route::post('/refresh', 'AuthController@refresh')->name('auth.refresh');
        \Route::post('/logout', 'AuthController@logout')->name('auth.logout');
    });
});


// For testing and debugging purpose only
if (env('APP_ENV') == 'testing') {
    \Route::group([
        'middleware' => ['api',],
    ], function () {
        \Route::get('/test', 'DummyController@getTest');
        \Route::post('/test', 'DummyController@postTest');
        \Route::middleware('auth:api')->get('/test/auth', 'DummyController@getTest');
    });
}
