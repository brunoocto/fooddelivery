Miso soup is a traditional Japanese soup consisting of a dashi stock into which softened miso paste is mixed.
In addition, there are many optional ingredients that may be added depending on regional and seasonal recipes, and personal preference.

2.00