import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import Signing from '@/views/auth/Signing.vue';

// describe('Signing.vue', () => {
//   it('should render the Login page', () => {
//     const action = 'login';
//     const wrapper = shallowMount(Signing, {
//       propsData: { action },
//     });
//     expect(wrapper.text()).to.include(msg);
//   });
// });

describe('Signing.vue', () => {
  it('should render the Login page', () => {
    const action = 'login';
    const wrapper = shallowMount(Signing, {
      propsData: { action },
    });
    expect(wrapper.find('.form-group').text()).to.contain('Full Name');
    // expect(wrapper.vm.isLoging).to.be.false;
  });
});
