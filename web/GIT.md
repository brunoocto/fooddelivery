# GIT Operations

The workflow is based on developers' name for simplicity (like "bruno").<br>
Ideally it should be better to use Jira ticket name for convention of branch naming (like "WL-142"), but it is more complicated to follow.

## -------- SETUP -------

### Set SSH Key to be able accessing gitlab repository without typing credentials
```bash
ssh-keygen -t rsa -b 4096 -C "firstname.lastname@example.com"
cat ~/.ssh/id_rsa.pub
```

### Set remote origin link
```bash
git remote set-url origin git@gitlab.com:groupname/projectname.git
git remote -v
```

### Create your local dev branch
```bash
git checkout -b bruno
```

### Sets the default remote branch for local branch
```bash
git fetch --prune
git branch -a
git push -u origin bruno
git branch -vv
```


## -------- PUSH COMMIT -------

### Create commit
```bash
git add -A
git commit -a
```

### Push commits
```bash
git push
```

Make a merge request if necessary then.

## -------- PULL MASTER -------

### Pull Rebase new code from master to your current branch (for instance if you were working on "bruno" here)
```bash
git stash save
git checkout master
git pull
# Possible conflicts (see below how)
git checkout bruno
git rebase master
git stash pop
```

### In case of conflict, check which files are concerned with git status (or visual software like gitk), correct them, save them, and add them
```bash
git add -A
git rebase --continue
```



