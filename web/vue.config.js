// It helps to avoid browser file cache in index.html
process.env.VUE_APP_TIMESTAMP = new Date().getTime();

module.exports = {
  devServer: {
    disableHostCheck: true,
  },

  pages: {
    index: {
      entry: 'src/main.js',
      title: 'Food Delivery',
    },
  },

  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import '@/styles/variables.scss';
          @import '@/plugins/bmResponsive/index.scss';
          @import '@/styles/global.scss';
        `,
      },
    },
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: true,
    },
  },
};
