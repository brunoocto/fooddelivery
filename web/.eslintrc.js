module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ['plugin:vue/essential', '@vue/airbnb'],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'max-len': 0,
    'no-underscore-dangle': 0,
    'vue/no-side-effects-in-computed-properties': 0,
    'no-undef': 0,
    'prefer-const': 0,
    eqeqeq: 0,
    'prefer-template': 0,
    'arrow-parens': 0,
    'no-restricted-syntax': 0,
    indent: 0,
    'guard-for-in': 0,
    'no-param-reassign': 0,
  },
  overrides: [
    {
      files: ['**/__tests__/*.{j,t}s?(x)', '**/tests/unit/**/*.spec.{j,t}s?(x)'],
      env: {
        mocha: true,
      },
    },
  ],
};
