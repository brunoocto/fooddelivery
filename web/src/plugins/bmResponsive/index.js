const bmResponsive = {
  install(Vue) {
    Vue.prototype.$thresholds = {
      isMobile: 'only screen and (max-width: 575px)',
      isMobileL: 'only screen and (min-width: 576px) and (max-width: 959px)',
      isTablet: 'only screen and (min-width: 960px) and (max-width: 1279px)',
      isDesktop: 'only screen and (min-width: 1280px)',

      noMobile: 'only screen and (min-width: 576px)',
      noMobileL: 'only screen and (min-width: 960px), only screen and (max-width: 575px)',
      noTablet: 'only screen and (min-width: 1280px), only screen and (max-width: 959px)',
      noDesktop: 'only screen and (max-width: 1279px)',

      minMobile: '',
      minMobileL: 'only screen and (min-width: 576px)',
      minTablet: 'only screen and (min-width: 960px)',
      minDesktop: 'only screen and (min-width: 1280px)',

      maxMobile: 'only screen and (max-width: 575px)',
      maxMobileL: 'only screen and (max-width: 959px)',
      maxTablet: 'only screen and (max-width: 1279px)',
      maxDesktop: '',
    };

    Vue.prototype.$breakpoints = [576, 960, 1280];

    Vue.prototype.$responsiveTest = function(threshold) {
      if (
        window.matchMedia &&
        this.$thresholds[threshold] &&
        window.matchMedia(this.$thresholds[threshold]).matches
      ) {
        return true;
      }
      return false;
    };
  },
};

export default bmResponsive;
