import Vue from 'vue';
import './plugins/fontawesome';
// You need to ensure that you are using css-loader
import '@fortawesome/fontawesome-free/css/all.css';
import jQuery from 'jquery';

// Lodash
import VueLodash from 'vue-lodash';
import lodash from 'lodash';

// Cookies
import vueCookies from 'vue-cookies';

// Encryption
import VueCryptojs from 'vue-cryptojs';

// Library to Query complex Json structure
import jsonata from 'jsonata';

// Axios
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueMoment from 'vue-moment';
import VueFaker from 'vue-faker';

import store from '@/store/index.js';
import router from '@/router';

// Global mixin
import mxGlobal from '@/mixins/global.js';

// Bootstrap
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import '@/styles/bootstrap.scss';

// Common Responsive features to attach on all components
import responsive from '@/plugins/bmResponsive/index.js';

// Directive to detect element resize
// import resize from 'vue-element-resize-detector';
import VueResizeObserver from 'vue-resize-observer';

// Tooltip if ellipsis
import Ellipsis from 'vue-directive-ellipsis';
import 'vue-directive-ellipsis/dist/ellipsis.umd.css';

import Scroll from '@/components/global/Scroll.vue';

import Upload from '@/components/global/Upload.vue';

import App from '@/App.vue';
import i18n from '@/i18n';

import VueUploadComponent from 'vue-upload-component';

global.jsonata = jsonata;

global.$ = jQuery;

Vue.config.productionTip = false;
Vue.use(VueLodash, { lodash });
Vue.use(VueAxios, axios);
Vue.use(VueMoment);
Vue.use(vueCookies);
Vue.use(VueCryptojs);
Vue.use(responsive);
// Install BootstrapVue
Vue.use(BootstrapVue);
// Install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);
Vue.use(VueResizeObserver);

Vue.component('scroll', Scroll);
Vue.component('file-upload', VueUploadComponent);
Vue.component('upload', Upload);

Vue.directive('ellipsis', Ellipsis);

// Faker
Vue.use(VueFaker);
// Global mixin
Vue.mixin(mxGlobal);

global.appVue = new Vue({
  router,
  store,

  created() {
    // Secure the cookie access
    this.$cookies.config('', '', '', true);
    // Initialize store data structure by submitting action.
    this.$store.dispatch('_auth/init');
  },

  i18n,
  render: h => h(App),
}).$mount('#app');
