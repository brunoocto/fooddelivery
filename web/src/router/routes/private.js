const routes = [
  // Welcome
  {
    path: '/welcome',
    name: 'Welcome',
    component: () => import('@/views/pages/Welcome.vue'),
    beforeEnter: async (to, from, next) => {
      // Only for Owner
      const role = await appVue.$store.dispatch('_app/getRole');
      // For regular user, we don't display a Welcome page, we go directly to the restaurants list
      if (role === 'regular') {
        return appVue.$router.push({
          name: 'Restaurants',
        });
      }
      return next();
    },
  },

  // Restaurants
  {
    path: '/restaurants',
    name: 'Restaurants',
    component: () => import('@/views/pages/Restaurants.vue'),
  },
  {
    path: '/restaurants/:id(\\d+)',
    props: true,
    name: 'Restaurant',
    component: () => {
      const role = appVue.$store.getters['_app/role'];
      if (role === 'owner') {
        return import('@/views/pages/owner/Restaurant.vue');
      }
      return import('@/views/pages/regular/Restaurant.vue');
    },
    beforeEnter: async (to, from, next) => {
      // Make sure the role is loaded
      await appVue.$store.dispatch('_app/getRole');
      // Check if we have the right to access it
      await appVue.$store.dispatch('restaurants/loadById', { id: to.params.id });
      let restaurant = appVue.$store.getters['restaurants/byId']({ id: to.params.id });
      if (restaurant == undefined) {
        appVue.$bvModal.show('modal-error');
        return appVue.$router.push({
          name: 'Restaurants',
        });
      }
      // Setup back button
      let backButton = appVue.$store.getters['_layout/backButton'];
      if (!backButton) {
        // If No previous app history
        await appVue.$store.dispatch('_layout/setBackButton', { name: 'Restaurants' });
      } else {
        await appVue.$store.dispatch('_layout/setBackButton', true);
      }
      return next();
    },
  },

  // Meals
  {
    path: '/meals',
    name: 'Meals',
    component: () => import('@/views/pages/owner/Meals.vue'),
    beforeEnter: async (to, from, next) => {
      // Only for Owner
      const role = await appVue.$store.dispatch('_app/getRole');
      // For regular user, we don't display a Welcome page, we go directly to the restaurants list
      if (role === 'regular') {
        return appVue.$router.push({
          name: 'Restaurants',
        });
      }
      return next();
    },
  },

  // Orders
  {
    path: '/orders',
    name: 'Orders',
    component: () => import('@/views/pages/Orders.vue'),
    beforeEnter: async (to, from, next) => {
      // Make sure the role is loaded
      const role = await appVue.$store.dispatch('_app/getRole');
      // Setup back button for Regular only
      if (role === 'regular') {
        let backButton = appVue.$store.getters['_layout/backButton'];
        if (!backButton) {
          // If No previous app history
          await appVue.$store.dispatch('_layout/setBackButton', { name: 'Restaurants' });
        } else {
          await appVue.$store.dispatch('_layout/setBackButton', true);
        }
      }
      return next();
    },
  },
];

export default routes.map(route => {
  let meta = {
    public: false,
    onlyLoggedOut: false,
    menu: null,
  };
  if (route.meta) {
    meta = { ...meta, ...route.meta };
  }
  return {
    ...route,
    meta,
  };
});
