const routes = [
  {
    path: '/',
    redirect: { name: 'Restaurants' },
  },
];

export default routes.map((route) => {
  let meta = {
    public: true,
    onlyLoggedOut: false,
    menu: null,
    displayPage: null,
  };
  if (route.meta) {
    meta = { ...meta, ...route.meta };
  }
  return {
    ...route,
    meta,
  };
});
