import Signing from '@/views/auth/Signing.vue';

const routes = [
  {
    path: '/auth/login',
    name: 'authLogin',
    components: {
      credentials: Signing,
    },
    props: {
      default: true,
      credentials: {
        action: 'login',
      },
    },
  },
  {
    path: '/auth/register',
    name: 'authRegister',
    components: {
      credentials: Signing,
    },
    props: {
      default: true,
      credentials: {
        action: 'register',
      },
    },
  },
];

export default routes.map((route) => {
  let meta = {
    public: true,
    onlyLoggedOut: true,
    menu: null,
    displayPage: null,
  };
  if (route.meta) {
    meta = { ...meta, ...route.meta };
  }
  return { ...route, meta };
});
