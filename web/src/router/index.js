import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store/index.js';
import allRoutes from '@/router/routes/all.js';
import publicRoutes from '@/router/routes/public.js';
import privateRoutes from '@/router/routes/private.js';

Vue.use(VueRouter);

const routes = [].concat(allRoutes, publicRoutes, privateRoutes);

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach(async (to, from, next) => {
  const authenticated = await store.dispatch('_auth/getAuthorization');
  const onlyLoggedOut = to.matched.some(record => record.meta.onlyLoggedOut);
  const isPublic = to.matched.some(record => record.meta.public);

  // WARNING: Do not break the logic here, it can lead to infinite loop!
  if (!isPublic && !authenticated) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    const everLogged = !!localStorage.getItem('auth.ever_logged');
    return router.push({
      name: everLogged ? 'authLogin' : 'authRegister',
      query: { redirect: to.fullPath },
    });
  }
  if (authenticated && onlyLoggedOut) {
    return router.push('/');
  }

  // Set variable for name display change
  let displayPage = null;
  // We use it only if the route name exists as language variable, otherwise we use Company name
  if (to.name !== undefined && appVue.$i18n.te('router.' + to.name)) {
    displayPage = appVue.$i18n.t('router.' + to.name);
  }

  // It helps to display the page loading logo, the timeout needs to be equal or higher that the fading which is 300ms
  store.dispatch('_layout/setLoadingRouteData', true);
  await new Promise(resolve => setTimeout(resolve, 400));

  store.dispatch('_layout/setDisplayPage', displayPage);
  store.dispatch('_layout/setPage', to.name);

  next();
});

export default router;
