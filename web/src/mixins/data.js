/**
 * This mixin is used to display a loading logo while loading a content page
 */
import { mapActions } from 'vuex';

export default {
  data() {
    return {
      idleTime: 10000, // Wait a minimum of 10s between any data refresh
      mxData_refreshBlock: false,
      mxData_refreshWait: false,
      mxData_interval: null,
    };
  },

  methods: {
    ...mapActions({
      _mxData_getUser: '_app/getUser',
      _mxData_restaurantsLoadLatest: 'restaurants/loadLatest',
      _mxData_mealsLoadLatest: 'meals/loadLatest',
      _mxData_ordersLoadLatest: 'orders/loadLatest',
    }),

    /**
     * It would be more relevant to use WebSocket to having the Client app informed of
     * any change operated on the Server side. But need more time for development to implement.
     */
    // Parallel calls
    async mxData_refreshData() {
      // Wait 15s
      if (this.mxData_blockRefresh) {
        this.mxData_refreshWait = true;
        return null;
      }
      this.mxData_blockRefresh = true;
      setTimeout(() => {
        this.mxData_blockRefresh = false;
      }, this.idleTime);
      // Begin calls and store promises without waiting
      const restaurants = this._mxData_restaurantsLoadLatest();
      const meals = this._mxData_mealsLoadLatest();
      const orders = this._mxData_ordersLoadLatest();
      const user = this._mxData_getUser();
      // Now we await for both results, whose async processes have already been started
      return [await restaurants, await meals, await orders, await user];
    },
  },

  watch: {
    mxData_blockRefresh() {
      // If the user click within 15s, we make calls again at the end of 15s
      if (this.mxData_refreshWait) {
        this.mxData_refreshWait = false;
        this.mxData_refreshData();
      }
    },
  },

  async beforeMount() {
    // Preload the data, we slow down the opening a bit, but avoid redundancy calls
    // NOTE: Not an ideal solution for heavy application in production! Need use Lazy loading instead.
    await this.mxData_refreshData();
  },

  mounted() {
    document.addEventListener('click', this.mxData_refreshData);
    document.addEventListener('focus', this.mxData_refreshData);
    document.addEventListener('mouseenter', this.mxData_refreshData);
    // Refresh all Data every 30 (even if idle)
    this.mxData_interval = setInterval(() => {
      this.mxData_refreshData();
    }, 1800 * 1000);
  },

  beforeDestroy() {
    document.removeEventListener('click', this.mxData_refreshData);
    document.removeEventListener('focus', this.mxData_refreshData);
    document.removeEventListener('mouseenter', this.mxData_refreshData);
    clearInterval(this.mxData_interval);
  },
};
