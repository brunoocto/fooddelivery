export default {
  methods: {
    /**
     * Return the list of related object
     * "data" can be 1 object or a array of object
     * mxJsonata_belongsTo({ 'restaurants': '1' });
     * mxJsonata_belongsTo({ 'restaurants': ['1', '3'] });
     * mxJsonata_belongsTo({ 'restaurants': ['1', '3'], 'meals': '11',});
     */
    mxJsonata_belongsTo(type, data) {
      let request = '';
      for (const relation in data) {
        if (request) {
          request += ' or ';
        }
        if (_.isArray(data[relation])) {
          // Convert vales to String
          data[relation] = data[relation].map(x => x.toString());
          request += `$string(relationships.${relation}.data.id) in [${data[relation].join()}]`;
        } else {
          request += `$string(relationships.${relation}.data.id) = $string(${data[relation]})`;
        }
      }
      // Build the Jsonata request
      if (request) {
        // WARNING: I need to use "appVue" and "type" because I don't know how to retreive from the function itself (this is different)
        const items = appVue.$store.getters[type + '/all'];
        return jsonata(`[items[${request}]]`).evaluate({ items });
      }

      return [];
    },
  },
};
