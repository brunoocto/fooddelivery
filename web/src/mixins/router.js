/**
 * This mixin is used to display a loading logo while loading a content page
 */
import { mapGetters, mapActions } from 'vuex';

export default {
  computed: {
    ...mapGetters({
      _loadingRouteData: '_layout/loadingRouteData',
    }),
    loadingRouteData: {
      set(data) {
        this._setLoadingRouteData(data);
      },
      get() {
        return this._loadingRouteData;
      },
    },
  },

  methods: {
    ...mapActions({ _setLoadingRouteData: '_layout/setLoadingRouteData' }),
  },

  mounted() {
    this.$nextTick(() => {
      this.loadingRouteData = false;
    });
  },
};
