import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';

import _app from '@/store/app/app.js';
import _layout from '@/store/app/layout.js';
import _auth from '@/store/app/auth.js';
import _upload from '@/store/app/upload.js';
import fooddelivery from '@/store/app/fooddelivery.js';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';
export default new Vuex.Store({
  modules: {
    _app,
    _layout,
    _auth,
    _upload,
    fooddelivery,
  },
  strict: debug,
  // plugins: debug? [ createLogger() ] : [],
});
