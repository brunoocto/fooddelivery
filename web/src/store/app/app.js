import axios from 'axios';

const httpClient = axios.create({
  baseURL: `${process.env.VUE_APP_API_URL}/api/fooddelivery/`,
  headers: {
    'Api-Key': process.env.VUE_APP_API_KEY,
    // Note: Content-Type is added by Axios only if data is defined, which mean that it won't be defined for a GET request
    'Content-Type': 'application/vnd.api+json',
    Accept: 'application/vnd.api+json',
  },
});

// Add Authorization Headers to all calls
httpClient.interceptors.request.use(async config => {
  config.headers.Authorization = await appVue.$store.dispatch('_auth/getAuthorization');
  return config;
});

// State object
const state = {
  user: null, // Logged user
  role: null, // (Server-Side data) The role linked to the access token
};

// Getter functions
const getters = {
  user(state) {
    return state.user;
  },
  role(state) {
    return state.role;
  },
};

// Actions
const actions = {
  reset({ commit }) {
    commit('SET_USER', null);
    commit('SET_ROLE', null);
  },

  async getUser({ getters, commit }, force) {
    if (force == undefined) {
      force = false;
    }
    // We check first if the suer has been retrieved
    if (!force && getters.user != null) {
      return getters.user;
    }
    // Check if the user is logged
    // We cannot use this.dispatch here because we are using async
    let authorization = await appVue.$store.dispatch('_auth/getAuthorization');
    if (authorization == null) {
      commit('SET_USER', null);
      return null;
    }
    await httpClient.get('/users/me').then(response => {
      // Store the user
      if (
        response.data &&
        response.data.data &&
        response.data.data.type &&
        response.data.data.type == 'users'
      ) {
        commit('SET_USER', response.data.data);
      }
      // Store the role
      if (
        response.data &&
        response.data.meta &&
        response.data.meta.data &&
        response.data.meta.data.as
      ) {
        commit('SET_ROLE', response.data.meta.data.as);
      }
    });

    return getters.user;
  },

  async getRole({ getters, dispatch }) {
    if (getters.role == null) {
      // Make sure we load at least once the user data from the serverr
      await dispatch('getUser');
    }
    // If the server does not respond, we temporary get the value from the Client
    if (getters.role == null && appVue.$store.getters['_auth/as']) {
      return appVue.$store.getters['_auth/as'];
    }
    return getters.role;
  },
};

// Mutations
const mutations = {
  SET_USER(state, data) {
    state.user = data;
  },
  SET_ROLE(state, data) {
    state.role = data;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
