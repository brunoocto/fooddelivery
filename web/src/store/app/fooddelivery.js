import axios from 'axios';
import { mapResourceModules } from '@reststate/vuex';

const httpClient = axios.create({
  baseURL: `${process.env.VUE_APP_API_URL}/api/fooddelivery/`,
  headers: {
    'Api-Key': process.env.VUE_APP_API_KEY,
    // Note: Content-Type is added by Axios only if data is defined, which mean that it won't be defined for a GET request
    'Content-Type': 'application/vnd.api+json',
    Accept: 'application/vnd.api+json',
  },
});

// Add Authorization Headers to all calls
httpClient.interceptors.request.use(async config => {
  config.headers.Authorization = await appVue.$store.dispatch('_auth/getAuthorization');
  return config;
});

const pictureURL = process.env.VUE_APP_STORAGE_URL;

// Mixin
const mixin = {
  state: {
    checktime: null, // Is fulfilled by getLatest only
    isSoftDelete: null, // At true if the field deleted_at is present
    type: null, // Type of the model
  },
  getters: {
    // the computed "checktime" can be used to avoid having a deep watch into the complete list
    checktime(state) {
      return state.checktime;
    },

    isSoftDelete(state) {
      return state.isSoftDelete;
    },

    type(state) {
      return state.type;
    },

    getRestaurantPicture: state => id => {
      let picture = require('@/assets/img/models/restaurant.jpg');
      let item = state.records.find(r => r.id == id);
      if (item && item.attributes && item.attributes.picture) {
        picture = `${pictureURL}/${item.attributes.picture}`;
      }
      return picture;
    },

    getMealPicture: state => id => {
      let picture = require('@/assets/img/models/meal.jpg');
      let item = state.records.find(r => r.id == id);
      if (item && item.attributes && item.attributes.picture) {
        picture = `${pictureURL}/${item.attributes.picture}`;
      }
      return picture;
    },
  },

  actions: {
    setChecktime({ commit }, data) {
      commit('SET_CHECKTIME', data);
    },

    async patch({ dispatch, commit }, data) {
      // Only for attributes, we can modify then immetialy
      commit('MERGE_ATTRIBUTES', { id: data.data.id, attributes: data.data.attributes });
      await httpClient
        .patch(`/${data.data.type}/${data.data.id}`, data)
        .then(async () => {
          await dispatch('loadLatest');
        })
        .catch(async () => {
          // In case of any error, we revert back to previous data
          commit('SET_CHECKTIME', null);
          // Reload the instance
          await dispatch('loadById', { id: data.data.id });
        });
    },

    async loadLatest({ commit, getters, dispatch }, data) {
      const checktime = parseInt(getters.checktime, 10);
      let call;
      let checkDeleted = false;
      const param = {
        filter: {},
        options: {},
      };

      if (_.isPlainObject(data)) {
        if (_.isPlainObject(data.filter)) {
          param.filter = data.filter;
        }
        if (_.isPlainObject(data.options)) {
          param.options = data.options;
        }
      }

      // We can get the latest only if the Model is a softDelete version, otherwise we won't be able to remove the deleted ones
      if (false && getters.isSoftDelete && _.isNumber(checktime)) {
        // updated_at >= checktime
        // In case updated_at does not exists, it simply do a all (with deletion)
        param.filter['updated_at, %3E%3D'] = checktime;
        param.filter['with-trashed'] = true;

        call = dispatch('loadWhere', param);
        checkDeleted = true;
      } else {
        // We don't need to check deleted items here since "loadAll" reset all items
        call = dispatch('loadAll', param);
        checkDeleted = false;
      }
      await call.then(() => {
        // Get all items received
        const items = getters.all;
        // Check if it is a softDelete Model
        if (getters.isSoftDelete == null) {
          if (items && items[0] && items[0].attributes) {
            if (items[0].attributes.deleted_at !== undefined) {
              commit('SET_IS_SOFT_DELETE', true);
            } else {
              commit('SET_IS_SOFT_DELETE', false);
            }
          }
        }

        // Set the type
        if (items && items[0] && items[0].type) {
          commit('SET_TYPE', items[0].type);

          // Get the highest checktime information
          let callChecktime = Math.max(
            getters.checktime,
            ...items.map(item => {
              if (item.meta && item.meta.checktime) {
                return item.meta.checktime;
              }
              return null;
            })
          );
          callChecktime = parseInt(callChecktime, 10);
          if (callChecktime) {
            commit('SET_CHECKTIME', callChecktime);
          }
        }

        // Delete any deleted items
        if (checkDeleted) {
          // https://docs.jsonata.org/using-nodejs
          const deletedItems = jsonata('[items[attributes.deleted_at!=null]]').evaluate({ items });

          if (_.isArray(deletedItems)) {
            deletedItems.forEach(item => {
              dispatch('removeRecord', item);
            });
          }
        }
      });
    },
  },

  mutations: {
    SET_CHECKTIME(state, data) {
      if (data == null) {
        state.checktime = null;
      } else {
        data = parseInt(data, 10);
        if (!_.isNaN(data) && _.isNumber(data) && data > 0) {
          state.checktime = data;
        }
      }
    },
    SET_IS_SOFT_DELETE(state, data) {
      if (typeof data === 'boolean') {
        state.isSoftDelete = data;
      }
    },
    SET_TYPE(state, data) {
      state.type = data;
    },
    MERGE_ATTRIBUTES(state, data) {
      let item = state.records.find(r => r.id == data.id);
      for (const i in data.attributes) {
        item.attributes[i] = data.attributes[i];
      }
    },
  },
};

// Exporting the module helps to use the namespace 'fooddelivery'
const modules = {
  ...mapResourceModules({
    names: ['users', 'restaurants', 'meals', 'orders'],
    httpClient,
  }),
};

for (const name in modules) {
  modules[name].state = { ...modules[name].state, ...mixin.state };
  modules[name].getters = { ...modules[name].getters, ...mixin.getters };
  modules[name].actions = { ...modules[name].actions, ...mixin.actions };
  modules[name].mutations = {
    ...modules[name].mutations,
    ...mixin.mutations,
  };
}

/*
  https://vuex.reststate.codingitwrong.com/reading-data.html#loadall-action-all-getter
  List of possible calls:
    Action {options} => Getter
    widgets/loadAll
    widgets/loadAll { options: { 'fields[widgets]': 'title,description', } } => widgets?fields[widgets]=title,description
    widgets/loadAll { options: { include: 'category,comments,comments.user', } }
    widgets/loadById { id: 42 }
    widgets/loadWhere { category: 'whizbang', }
    widgets/loadPage { 'page[size]': 10, 'page[number]': 2, }
    widgets/loadRelated { type: 'category', id: '27' } => (categories is parent) categories/27/widgets
    widgets/loadRelated { type: 'category', id: '27' }, 'purchased-widgets' =>  (different parent naming) purchased-widgets/27/widgets
    widgets/create { attributes: { title: 'My Widget', }, }
    widgets/update widget(the getter object)
    widgets/delete { id: 42 }
*/
export default {
  // We cannot use namespace, otherwise "?include=" won't work
  namespaced: false,
  modules,
};
