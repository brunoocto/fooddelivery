// State object
const state = {
  appSize: null, // Store responsive string key
  navigationDrawer: false,
  backButton: false,
  page: null,
  displayPage: null,
  loadingRouteData: false, // Indicate if the page rendering needs to wait
};

// Getter functions
const getters = {
  appSize(state) {
    return state.appSize;
  },
  navigationDrawer(state) {
    return state.navigationDrawer;
  },
  backButton(state) {
    return state.backButton;
  },
  page(state) {
    return state.page;
  },
  displayPage(state) {
    return state.displayPage;
  },
  loadingRouteData(state) {
    return state.loadingRouteData;
  },
};

// Actions
const actions = {
  setAppSize({ commit }, data) {
    commit('SET_APP_SIZE', data);
  },
  setNavigationDrawer({ commit }, data) {
    if (typeof data === 'boolean') {
      commit('SET_NAVIGATION_DRAWER', data);
    }
  },
  setBackButton({ commit }, data) {
    commit('SET_BACK_BUTTON', data);
  },
  setPage({ commit, dispatch }, data) {
    commit('SET_PAGE', data);
    dispatch('setDisplayPage', data);
  },
  setDisplayPage({ commit, getters }, data) {
    if (data == undefined) {
      // We use it only if the route name exists as language variable, otherwise we use Company name
      if (getters.page != null && appVue.$i18n.te('router.' + getters.page)) {
        data = appVue.$i18n.t('router.' + getters.page);
      }
    }
    if (!_.isString(data)) {
      data = 'Food Delivery';
    }
    commit('SET_DISPLAY_PAGE', data);
  },
  setLoadingRouteData({ commit }, data) {
    if (typeof data === 'boolean') {
      commit('SET_LOADING_ROUTE_DATA', data);
    }
  },
};

// Mutations
const mutations = {
  SET_APP_SIZE(state, data) {
    state.appSize = data;
  },
  SET_NAVIGATION_DRAWER(state, data) {
    state.navigationDrawer = data;
  },
  SET_BACK_BUTTON(state, data) {
    state.backButton = data;
  },
  SET_PAGE(state, data) {
    state.page = data;
  },
  SET_DISPLAY_PAGE(state, data) {
    state.displayPage = data;
  },
  SET_LOADING_ROUTE_DATA(state, data) {
    state.loadingRouteData = data;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
