import axios from 'axios';
import moment from 'moment';

const httpClient = axios.create({
  baseURL: `${process.env.VUE_APP_API_URL}/api/auth`,
  headers: {
    'Api-Key': process.env.VUE_APP_API_KEY,
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

// For staging/production 'Operator' is filled by nginx via the subdomain name (or other method)
if (process.env.NODE_ENV === 'development') {
  httpClient.defaults.headers.Operator = process.env.VUE_APP_DEV_OPERATOR;
}

// State object
const state = {
  tokenType: null, // Type of Token used to identify the user
  accessToken: null, // Token to access the API on behalf of the logged user
  expiresAt: null, // Timestamp of estimated expiration date
  refreshToken: null, // Token to refresh the access token
  rememberMe: false, // Do not remember by default
  as: 'regular', // (Client-Side data) Log as a regular user by default (Customer)
};

// Getter functions
const getters = {
  tokenType(state) {
    return state.tokenType;
  },
  accessToken(state) {
    return state.accessToken;
  },
  expiresAt(state) {
    return state.expiresAt;
  },
  refreshToken(state) {
    return state.refreshToken;
  },
  rememberMe(state) {
    return state.rememberMe;
  },
  as(state) {
    return state.as;
  },
  authorization(state) {
    if (state.tokenType && state.accessToken) {
      return `${state.tokenType} ${state.accessToken}`;
    }
    return null;
  },
};

// Actions
const actions = {
  setRememberMe({ commit }, data) {
    // Force to convert to boolean if not yet
    data = !!data;
    commit('SET_REMEMBER_ME', data);
    localStorage.setItem('auth.remember_me', data);
  },

  setAs({ commit }, data) {
    if (['regular', 'owner'].includes(data)) {
      commit('SET_AS', data);
      localStorage.setItem('auth.as', data);
    }
  },

  init({ commit, getters, dispatch }) {
    // Remember me
    if (localStorage.getItem('auth.remember_me')) {
      commit('SET_REMEMBER_ME', localStorage.getItem('auth.remember_me'));
    } else {
      // Set in localStorage the default value
      localStorage.setItem('auth.remember_me', getters.rememberMe);
    }

    // The user role
    if (localStorage.getItem('auth.as')) {
      commit('SET_AS', localStorage.getItem('auth.as'));
    } else {
      // Set in localStorage the default value
      localStorage.setItem('auth.as', getters.as);
    }

    const auth = {
      token_type: null,
      access_token: null,
      expires_at: null,
      refresh_token: null,
    };

    if (
      getters.rememberMe &&
      localStorage.getItem('auth.token_type') &&
      localStorage.getItem('auth.access_token') &&
      localStorage.getItem('auth.expires_at') &&
      localStorage.getItem('auth.refresh_token')
    ) {
      // Get localStorage when use RememberMe feature
      auth.token_type = localStorage.getItem('auth.token_type');
      auth.access_token = localStorage.getItem('auth.access_token');
      auth.expires_at = localStorage.getItem('auth.expires_at');
      auth.refresh_token = localStorage.getItem('auth.refresh_token');
    } else if (this._vm.$cookies.isKey('auth')) {
      // Get cookie for active Browser session
      let cookie_auth = null;
      const encrypted = this._vm.$cookies.get('auth');
      if (encrypted) {
        cookie_auth = JSON.parse(
          this._vm.CryptoJS.AES.decrypt(
            encrypted,
            process.env.VUE_APP_ENCRYPTION_PASSWORD
          ).toString(this._vm.CryptoJS.enc.Utf8)
        );
      }
      if (
        cookie_auth &&
        cookie_auth.token_type &&
        cookie_auth.access_token &&
        cookie_auth.expires_at &&
        cookie_auth.refresh_token
      ) {
        auth.token_type = cookie_auth.token_type;
        auth.access_token = cookie_auth.access_token;
        auth.expires_at = cookie_auth.expires_at;
        auth.refresh_token = cookie_auth.refresh_token;
      }
    }

    if (
      auth.token_type &&
      auth.access_token &&
      auth.expires_at &&
      auth.expires_at > moment().unix() &&
      auth.refresh_token
    ) {
      commit('SET_TOKEN_TYPE', auth.token_type);
      commit('SET_ACCESS_TOKEN', auth.access_token);
      commit('SET_EXPIRES_AT', auth.expires_at);
      commit('SET_REFRESH_TOKEN', auth.refresh_token);
    } else {
      dispatch('reset');
    }
  },

  async getAuthorization({ commit, getters, dispatch }) {
    // If authorization is not set, it certainly be the first call, so we init the variables with localStorage
    if (!getters.authorization) {
      dispatch('init');
    }

    // Return the state value
    if (
      getters.tokenType &&
      getters.accessToken &&
      getters.expiresAt &&
      getters.expiresAt > moment().unix()
    ) {
      return getters.authorization;
    }
    commit('SET_TOKEN_TYPE', null);
    commit('SET_ACCESS_TOKEN', null);

    // If the token is expired, we refresh it
    if (getters.refreshToken && getters.expiresAt && getters.expiresAt <= moment().unix()) {
      await dispatch('refresh');
      if (etters.tokenType && getters.accessToken) {
        return getters.authorization;
      }
    }
    commit('SET_REFRESH_TOKEN', null);
    commit('SET_EXPIRES_AT', null);

    return null;
  },

  async register({ dispatch }, data) {
    // We first clear
    dispatch('reset');
    // Save in localStorage the role selected
    if (data.as) {
      dispatch('setAs', data.as);
    }
    // Do refresh the token
    const result = await httpClient
      .post('/register', data)
      .then(response => {
        dispatch('set', response);
        return response;
      })
      .catch(error => {
        dispatch('reset');
        return Promise.reject(error);
      });

    return result;
  },

  async login({ dispatch }, data) {
    // We first clear
    dispatch('reset');
    // Save in localStorage the role selected
    if (data.as) {
      dispatch('setAs', data.as);
    }
    // Do refresh the token
    const result = await httpClient
      .post('/login', data)
      .then(response => {
        dispatch('set', response);
        return response;
      })
      .catch(error => {
        dispatch('reset');
        return Promise.reject(error);
      });

    return result;
  },

  async refresh({ getters, dispatch }) {
    let result = null;
    if (getters.refreshToken) {
      // Do refresh the token
      result = await httpClient
        .post(
          '/refresh',
          {
            refresh_token: getters.refreshToken,
          },
          {
            headers: {
              Authorization: getters.authorization,
            },
          }
        )
        .then(response => {
          dispatch('set', response);
          return response;
        })
        .catch(error => {
          dispatch('reset');
          // Force to reload the page if the refreshing fail
          window.location.reload(true);
          return Promise.reject(error);
        });
    } else {
      dispatch('reset');
    }
    return result;
  },

  logout({ getters, dispatch }) {
    let result = null;
    if (getters.accessToken) {
      // Do refresh the token
      result = httpClient.post(
        '/logout',
        {
          access_token: getters.accessToken,
        },
        {
          headers: {
            Authorization: getters.authorization,
          },
        }
      );
    }
    dispatch('reset');
    return result;
  },

  set({ commit, getters }, response) {
    commit('SET_TOKEN_TYPE', response.data.token_type);
    commit('SET_ACCESS_TOKEN', response.data.access_token);
    const expiresAt = moment().unix() + response.data.expires_in;
    commit('SET_EXPIRES_AT', expiresAt);
    commit('SET_REFRESH_TOKEN', response.data.refresh_token);
    // If the user wants to remember the credentials between
    if (getters.rememberMe) {
      localStorage.setItem('auth.token_type', response.data.token_type);
      localStorage.setItem('auth.access_token', response.data.access_token);
      localStorage.setItem('auth.expires_at', expiresAt);
      localStorage.setItem('auth.refresh_token', response.data.refresh_token);
    }
    // In all the case we use Session Cookie to stay logged within a browser instance (page refresh and tabs)
    const auth = JSON.stringify({
      token_type: response.data.token_type,
      access_token: response.data.access_token,
      expires_at: expiresAt,
      refresh_token: response.data.refresh_token,
    });

    const encrytedAuth = this._vm.CryptoJS.AES.encrypt(
      auth,
      process.env.VUE_APP_ENCRYPTION_PASSWORD
    ).toString();
    // The expiration is 'Session' to use Session cookie
    this._vm.$cookies.set('auth', encrytedAuth, 'Session');

    // Keep track that the user ever logged on this computer
    localStorage.setItem('auth.ever_logged', true);
  },

  reset({ commit }) {
    // token_type
    commit('SET_TOKEN_TYPE', null);
    localStorage.removeItem('auth.token_type');
    // access_token
    commit('SET_ACCESS_TOKEN', null);
    localStorage.removeItem('auth.access_token');
    // exipires_at
    commit('SET_EXPIRES_AT', null);
    localStorage.removeItem('auth.expires_at');
    // refresh_token
    commit('SET_REFRESH_TOKEN', null);
    localStorage.removeItem('auth.refresh_token');
    // Clean the Authentication Cookie
    this._vm.$cookies.remove('auth');
    // Clean the data on app Store
    this.dispatch('_app/reset');
    // Clean the data on fooddelivery store
    const items = this.state.fooddelivery;
    for (const model in items) {
      this.dispatch(model + '/resetState');
      this.dispatch(model + '/setChecktime', null);
    }
  },
};

// Mutations
const mutations = {
  SET_TOKEN_TYPE(state, data) {
    state.tokenType = data;
  },
  SET_ACCESS_TOKEN(state, data) {
    state.accessToken = data;
  },
  SET_EXPIRES_AT(state, data) {
    state.expiresAt = data;
  },
  SET_REFRESH_TOKEN(state, data) {
    state.refreshToken = data;
  },
  SET_REMEMBER_ME(state, data) {
    // Force as boolean
    state.rememberMe = !!data;
  },
  SET_AS(state, data) {
    state.as = data;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
