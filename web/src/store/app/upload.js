// State object
const state = {
  files: {}, // Store uploading files
  refresh: 0,
};

// Getter functions
const getters = {
  files(state) {
    // It helps to refresh computed "files"
    state.refresh;
    return state.files;
  },
};

// Actions
const actions = {
  setFile({ commit }, data) {
    commit('SET_FILE', data);
  },
  removeFile({ commit }, data) {
    commit('REMOVE_FILE', data);
  },
};

// Mutations
const mutations = {
  SET_FILE(state, file) {
    if (file && file.id) {
      state.files[file.id] = file;
      state.refresh++;
    }
  },
  REMOVE_FILE(state, file) {
    if (file && file.id && state.files[file.id]) {
      state.files[file.id] = null;
      delete state.files[file.id];
      state.refresh++;
    }
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
