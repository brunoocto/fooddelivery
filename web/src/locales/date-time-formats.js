const dateTimeFormats = {
  en: {
    short: { year: 'numeric', month: 'short', day: 'numeric' },
    medium: {
      day: 'numeric',
      month: 'short',
      hour: 'numeric',
      minute: 'numeric',
      hour12: true,
    },
  },
  fr: {
    short: { year: 'numeric', month: 'long', day: 'numeric' },
    medium: {
      day: 'numeric',
      month: 'short',
      hour: 'numeric',
      minute: 'numeric',
    },
  },
};
export default dateTimeFormats;
