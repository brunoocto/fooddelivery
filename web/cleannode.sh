#!/bin/bash

####
# Modified    : November 24th, 2020
# Created     : November 24th, 2020
# Author      : Bruno Martin
# Email       : brunoocto@gmail.com
# Description : Kill Node scripts
# Sample      :	bash cleannode.sh
####

# Kill npm
pid=$(pidof npm)
kill -9 $pid

# Kill node
pid=$(pidof node)
kill -9 $pid

echo "Ready"