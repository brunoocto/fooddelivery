#!/bin/bash

####
# Modified    : November 16th, 2020
# Created     : November 16th, 2020
# Author      : Bruno Martin
# Email       : brunoocto@gmail.com
# Company     : Lincko
# Description : Clean composer to resync composer.lock with composer.json
# Sample      :	bash cleancomposer.sh
####

# Define the working path
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

# Color definition
RED='\e[91m'
BRED='\e[41m'
GREEN='\e[32m'
NC='\e[0m' # No Color

rm -rf vendor
rm composer.lock
composer clear-cache
composer install

php artisan cache:clear
php artisan route:clear
php artisan config:clear
php artisan view:clear
composer dump-autoload
