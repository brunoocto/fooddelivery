<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Passport\Client as OauthClients;
use App\Models\Users;

abstract class TestCase extends BaseTestCase
{
    /**
     * We cannot use refreshDatabase in memory here because of the following bugs:
     * 1) In the same "test" method, if we call more than once a request ->json(), any more call will fail because of some require_once in the Laravel boostrap. So we use refreshApplication();
     * 2) We can use $this->refreshApplication(), but it does launch the transaction rollback, so refreshDatabase cannot be used. So we use DatabaseMigrations.
     * 3) If we use DatabaseMigrations with Memory, refreshApplication delete the database, but not file version. So we use file version.
     */
    use CreatesApplication, DatabaseMigrations;
    
    /**
     * Keep trak of Oauth Client
     *
     * @var Laravel\Passport\Client
     */
    protected $oauth_client = null;

    /**
     * Instance of faker
     * Help to fake input data
     *
     * @var Faker\Generator
     */
    protected $faker;

    /**
     * User used as Authenticate user
     *
     * @var App\Models\Users
     */
    protected $auth_user = null;

    /**
     * Password used to authenticate the user
     *
     * @var string
     */
    protected $pwd = 'q1w2e3r4t5y6';

    /**
     * Store any kind of data to share between calls in a single test
     *
     * @var Array
     */
    protected $data = [];

    /**
     * Setup launched at every test method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        // Seed data
        \Artisan::call('db:seed', ['-q' => true]);

        // Get the OAuth Client
        $this->oauth_client = OauthClients::Where('password_client', 1)->first();

        $_ENV['PASSPORT_PASSWORD_GRANT_CLIENT_ID'] = $this->oauth_client->id;
        $_ENV['PASSPORT_PASSWORD_GRANT_CLIENT_SECRET'] = $this->oauth_client->secret;
        
        // Use Admin because he has all 3 roles
        $this->auth_user = Users::Where('email', 'admin@lincko.cafe')->first();

        // Empty the shared data
        $this->data = [];

        // LinckoJson error method does not sent output while testing, it does not stop the code as expected. We need to force to stop the code by throwing an Exception. Just be aware that in such case the output does not reflect what the error should return.
        // https://emarketbot.readthedocs.io/en/latest/reference/expectations.html
        \LinckoJson::shouldReceive('error')->atLeast()->times(0)->andThrow(new \Exception('LinckoJson::error Mockery Exception', 400));
    }

    /**
     * Log
     *
     * @param string $as the Role to authentcate the user
     * @return void
     */
    protected function login($as = 'admin')
    {
        if (!isset($this->data[$as])) {
            // Set the array for requested role
            $this->data[$as] = [];
        }
        // Only log once
        if (!isset($this->data[$as]['access_token']) || !isset($this->data[$as]['refresh_token'])) {
            // Success
            $response = $this->json(
                'POST',
                '/api/auth/login',
                [
                    'email' => $this->auth_user->email,
                    'password' => $this->pwd,
                    'as' => $as,
                ],
                [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Api-Key' => env('API_KEY'),
                ]
            );
            
            // Merge the data
            $this->data[$as] = array_merge($this->data[$as], (array)$response->json());
        }
    }

    /**
     * Check the basic structure of a Vnd Response with a single object
     *
     * @param Response $response  Json response of a call
     * @param string $type  Model type name
     * @return void
     */
    protected function commonSingleVndCheck($response, $type)
    {
        // Check the Status (2xx)
        $response->assertSuccessful();

        // Check the type is VND
        $response->assertHeader('Content-Type', 'application/vnd.api+json');

        // Check the minimum required Body structure
        $response->assertJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'created_at',
                    'updated_at',
                ],
                'links' => [
                    'self',
                ],
                'meta' => [
                    'checktime',
                ],
            ],
        ]);

        // Check if the type is correct
        $response->assertJson([
            'data' => [
                'type' => $type,
            ],
        ]);
    }

    /**
     * Check the basic structure of a Vnd Response with multiple objects
     *
     * @param Response $response  Json response of a call
     * @param string $type  Model type name
     * @return void
     */
    protected function commonMultipleVndCheck($response, $type)
    {
        // Check the Status (2xx)
        $response->assertSuccessful();

        // Check the type is VND
        $response->assertHeader('Content-Type', 'application/vnd.api+json');

        // Get number of users
        $count = count($response->json()['data']);
        // Prepare the structure to check
        $data = [];
        for ($i=0; $i < $count; $i++) {
            $data[] = [
                'type',
                'id',
                'attributes' => [
                    'created_at',
                    'updated_at',
                ],
                'links' => [
                    'self',
                ],
                'meta' => [
                    'checktime',
                ],
            ];
        }

        // Check the minimum required Body structure
        $response->assertJsonStructure([
            'data' => $data,
        ]);

        // Prepare the structure to check
        $data = [];
        for ($i=0; $i < $count; $i++) {
            $data[] = [
                'type' => $type,
            ];
        }

        // Check if the ype is correct
        $response->assertJson([
            'data' => $data,
        ]);
    }
}
