<?php

namespace Tests\Feature;

use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * Register
     *
     * @return void
     */
    public function testRegister()
    {

        // Success
        $response = $this->json(
            'POST',
            '/api/auth/register',
            [
                'name' => 'Bruno Martin',
                'email' => 'test@lincko.cafe',
                'password' => '123456',
                'password_confirmation' => '123456',
                'as' => 'owner',
            ],
            [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Api-Key' => env('API_KEY'),
            ]
        );

        // Check the structure
        $response->assertJsonStructure([
            'token_type',
            'expires_in',
            'access_token',
            'refresh_token',
        ]);

        // Check status
        $response->assertStatus(201);
    }

    /**
     * Register (Fail)
     *
     * @return void
     */
    public function testRegisterFail()
    {

        // Fail (Passwords do not match)
        $response = $this->json(
            'POST',
            '/api/auth/register',
            [
                'name' => 'Bruno Martin',
                'email' => 'test@lincko.cafe',
                'password' => '12345',
                'password_confirmation' => '1234567',
                'as' => 'owner',
            ],
            [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Api-Key' => env('API_KEY'),
            ]
        );

        // Check status
        $response->assertStatus(422);

        // Fail (Missing Password confirmation)
        $response = $this->json(
            'POST',
            '/api/auth/register',
            [
                'name' => 'Bruno Martin',
                'email' => 'test@lincko.cafe',
                'password' => '123456',
                'as' => 'owner',
            ],
            [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Api-Key' => env('API_KEY'),
            ]
        );

        // Check status
        $response->assertStatus(422);

        // Fail (Missing Role)
        $response = $this->json(
            'POST',
            '/api/auth/register',
            [
                'name' => 'Bruno Martin',
                'email' => 'test@lincko.cafe',
                'password' => '123456',
                'password_confirmation' => '123456',
            ],
            [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Api-Key' => env('API_KEY'),
            ]
        );

        // Check status
        $response->assertStatus(422);

        // Fail (Wrong role)
        $response = $this->json(
            'POST',
            '/api/auth/register',
            [
                'name' => 'Bruno Martin',
                'email' => 'test@lincko.cafe',
                'password' => '123456',
                'password_confirmation' => '123456',
                'as' => 'fake',
            ],
            [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Api-Key' => env('API_KEY'),
            ]
        );

        // Check status
        $response->assertStatus(422);
    }

    /**
     * Login
     *
     * @return void
     */
    public function testLogin()
    {
        $roles = ['admin', 'owner', 'regular'];
        foreach ($roles as $role) {
            // Success
            $response = $this->json(
                'POST',
                '/api/auth/login',
                [
                    'email' => $this->auth_user->email,
                    'password' => $this->pwd,
                    'as' => $role,
                ],
                [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'Api-Key' => env('API_KEY'),
                ]
            );

            // Check the structure
            $response->assertJsonStructure([
                'token_type',
                'expires_in',
                'access_token',
                'refresh_token',
            ]);

            // Check status
            $response->assertStatus(200);
        }
    }

    /**
     * Login (Fail)
     *
     * @return void
     */
    public function testLoginFail()
    {
        // Fail (Password too short)
        $response = $this->json(
            'POST',
            '/api/auth/login',
            [
                'email' => $this->auth_user->email,
                'password' => '12345',
                'as' => 'regular',
            ],
            [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Api-Key' => env('API_KEY'),
            ]
        );

        // Check status
        $response->assertStatus(422);

        // Fail (Wrong password)
        $response = $this->json(
            'POST',
            '/api/auth/login',
            [
                'email' => $this->auth_user->email,
                'password' => '1234567',
                'as' => 'regular',
            ],
            [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Api-Key' => env('API_KEY'),
            ]
        );

        // Check status
        $response->assertStatus(400);
    }

    /**
     * Refresh token
     *
     * @return void
     */
    public function testRefresh()
    {
        // Login
        $this->login('admin');

        // Success
        $response = $this->json(
            'POST',
            '/api/auth/refresh',
            [
                'refresh_token' => $this->data['admin']['refresh_token'],
            ],
            [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => $this->data['admin']['token_type'].' '.$this->data['admin']['access_token'],
            ]
        );

        // Check the structure
        $response->assertJsonStructure([
            'token_type',
            'expires_in',
            'access_token',
            'refresh_token',
        ]);

        // Check status
        $response->assertStatus(200);
    }

    /**
     * Logout
     *
     * @return void
     */
    public function testLogout()
    {
        // Login
        $this->login('admin');

        // Success
        $response = $this->json(
            'GET',
            '/api/test/auth',
            [],
            [
                'Accept' => 'application/json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => $this->data['admin']['token_type'].' '.$this->data['admin']['access_token'],
            ]
        );

        // Check Content
        $response->assertSeeText('getTest');

        // Check status
        $response->assertStatus(200);

        // Success
        $response = $this->json(
            'POST',
            '/api/auth/logout',
            [],
            [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => $this->data['admin']['token_type'].' '.$this->data['admin']['access_token'],
            ]
        );
        
        // Check status
        $response->assertStatus(204);
    }
}
