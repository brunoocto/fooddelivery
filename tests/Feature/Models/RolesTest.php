<?php

namespace Tests\Feature\Models;

use Tests\TestCase;
use App\Models\Roles;
use App\Models\Permissions;

class RolesTest extends TestCase
{
    /**
     * Test POST
     *
     * @return void
     */
    public function testPost()
    {
        // Login
        $this->login('admin');

        // Success
        $response = $this->json(
            'POST',
            '/api/fooddelivery/roles',
            [
                'data' => [
                    'type' => 'roles',
                    'attributes' => [
                        'name' => 'boss',
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'roles');
    }

    /**
     * Test GET all
     *
     * @return void
     */
    public function testGetAll()
    {
        // Login
        $this->login('admin');

        $count_ori = Roles::count();

        // Create 1 more item
        Roles::create([
            'name' => 'boss',
        ]);

        $response = $this->json(
            'GET',
            '/api/fooddelivery/roles',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonMultipleVndCheck($response, 'roles');

        // Check total number
        $count_new = count($response->json()['data']);
        $this->assertEquals($count_new, $count_ori + 1);
    }

    /**
     * Test GET
     *
     * @return void
     */
    public function testGet()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        $permission = Permissions::create([
            'role_id' => $role->id,
            'model' => 'meals',
        ]);

        // This should return a Roles
        $response = $this->json(
            'GET',
            '/api/fooddelivery/roles/'.$role->id,
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'roles');
    }

    /**
     * Test PATCH
     *
     * @return void
     */
    public function testPatch()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        // Patch
        $response = $this->json(
            'PATCH',
            '/api/fooddelivery/roles/'.$role->id,
            [
                'data' => [
                    'type' => 'roles',
                    'id' => $role->id,
                    'attributes' => [
                        'name' => 'patron',
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'roles');

        // Check some values
        $response->assertJson([
            'data' => [
                'type' => 'roles',
                'attributes' => [
                    'name' => 'patron',
                ],
            ],
        ]);
    }

    /**
     * Test PATCH Fail
     *
     * @return void
     */
    public function testPatchFail()
    {
        // Login
        $this->login('owner');

        // Create a new Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        // Patch
        $response = $this->json(
            'PATCH',
            '/api/fooddelivery/roles/'.$role->id,
            [
                'data' => [
                    'type' => 'roles',
                    'id' => $role->id,
                    'attributes' => [
                        'name' => 'patron',
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['owner']['access_token'],
            ]
        );

        // Check that an expection is sent
        $this->assertTrue(isset($response->exception));
    }

    /**
     * Test DELETE (soft)
     *
     * @return void
     */
    public function testDelete()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        // Delete
        $response = $this->json(
            'DELETE',
            '/api/fooddelivery/roles/'.$role->id,
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check Deletion status
        $response->assertStatus(204);

        // Get the deleted item
        $role = Roles::withTrashed()->find($role->id);

        // Check that the object exists
        $this->assertNotNull($role);

        // Check that it is soft deleted
        $this->assertNotNull($role->deleted_at);
    }

    /**
     * Test DELETE (hard)
     *
     * @return void
     */
    public function testDeleteForce()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        // Force to delete
        $response = $this->json(
            'PUT',
            '/api/fooddelivery/roles/'.$role->id.'/force_delete',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Item deleted
        $response->assertStatus(204);

        // Try to get the deleted item
        $role = Roles::withTrashed()->find($role->id);

        // Check that the object is deleted
        $this->assertNull($role);
    }

    /**
     * Test RESTORE
     *
     * @return void
     */
    public function testRestore()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        // Delete
        $role->delete();

        // Restore
        $response = $this->json(
            'PUT',
            '/api/fooddelivery/roles/'.$role->id.'/restore',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'roles');

        // Check Read status
        $response->assertStatus(200);

        // Check some values
        $response->assertJson([
            'data' => [
                'type' => 'roles',
                'id' =>  $role->id,
            ],
        ]);
    }
}
