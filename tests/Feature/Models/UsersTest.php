<?php

namespace Tests\Feature\Models;

use Tests\TestCase;
use App\Models\Roles;
use App\Models\Users;
use Illuminate\Support\Facades\Hash;

class UsersTest extends TestCase
{
    /**
     * Test POST
     *
     * @return void
     */
    public function testPost()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        // Success
        $response = $this->json(
            'POST',
            '/api/fooddelivery/users',
            [
                'data' => [
                    'type' => 'users',
                    'attributes' => [
                        'name' => 'Bruno Admin',
                        'email' => 'testing@lincko.cafe',
                        'password' => '123456',
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'users');
    }

    /**
     * Test GET all
     *
     * @return void
     */
    public function testGetAll()
    {
        // Login
        $this->login('admin');

        $count_ori = Users::count();

        // Create a new Item
        Users::create([
            'name' => 'Bruno Admin',
            'email' => 'testing@lincko.cafe',
            'password' => Hash::make('123456'),
        ]);

        $response = $this->json(
            'GET',
            '/api/fooddelivery/users',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonMultipleVndCheck($response, 'users');

        // Check total number
        $count_new = count($response->json()['data']);
        $this->assertEquals($count_new, $count_ori + 1);
    }

    /**
     * Test GET
     *
     * @return void
     */
    public function testGet()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $user = Users::create([
            'name' => 'Bruno Admin',
            'email' => 'testing@lincko.cafe',
            'password' => Hash::make('123456'),
        ]);

        // This should return a Users
        $response = $this->json(
            'GET',
            '/api/fooddelivery/users/'.$user->id,
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'users');
    }

    /**
     * Test PATCH
     *
     * @return void
     */
    public function testPatch()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $user = Users::create([
            'name' => 'Bruno Admin',
            'email' => 'testing@lincko.cafe',
            'password' => Hash::make('123456'),
        ]);

        // Patch
        $response = $this->json(
            'PATCH',
            '/api/fooddelivery/users/'.$user->id,
            [
                'data' => [
                    'type' => 'users',
                    'id' => $user->id,
                    'attributes' => [
                        'name' => 'Bruno MARTIN',
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'users');

        // Check some values
        $response->assertJson([
            'data' => [
                'type' => 'users',
                'attributes' => [
                    'name' => 'Bruno MARTIN',
                ],
            ],
        ]);
    }

    /**
     * Test PATCH Fail
     *
     * @return void
     */
    public function testPatchFail()
    {
        // Login
        $this->login('owner');

        // Create a new Item
        $user = Users::create([
            'name' => 'Bruno Admin',
            'email' => 'testing@lincko.cafe',
            'password' => Hash::make('123456'),
        ]);

        // Patch
        $response = $this->json(
            'PATCH',
            '/api/fooddelivery/users/'.$user->id,
            [
                'data' => [
                    'type' => 'users',
                    'id' => $user->id,
                    'attributes' => [
                        'name' => 'Bruno MARTIN',
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['owner']['access_token'],
            ]
        );
        
        // Check that an expection is sent
        $this->assertTrue(isset($response->exception));
    }

    /**
     * Test DELETE (soft)
     *
     * @return void
     */
    public function testDelete()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $user = Users::create([
            'name' => 'Bruno Admin',
            'email' => 'testing@lincko.cafe',
            'password' => Hash::make('123456'),
        ]);

        // Delete
        $response = $this->json(
            'DELETE',
            '/api/fooddelivery/users/'.$user->id,
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );
        
        // Check that an expection is sent
        $this->assertTrue(isset($response->exception));
    }
}
