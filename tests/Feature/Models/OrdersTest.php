<?php

namespace Tests\Feature\Models;

use Tests\TestCase;
use App\Models\Meals;
use App\Models\Restaurants;
use App\Models\Orders;

class OrdersTest extends TestCase
{
    /**
     * Test POST
     *
     * @return void
     */
    public function testPost()
    {
        // Login
        $this->login('regular');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        $meal = Meals::create([
            'restaurant_id' => $restaurant->id,
            'name' => 'Pasta',
            'description' => 'Best in Italy',
            'price' => 5.75,
        ]);
        
        // Success
        $response = $this->json(
            'POST',
            '/api/fooddelivery/orders',
            [
                'data' => [
                    'type' => 'orders',
                    'attributes' => [
                        'staus' => 1,
                    ],
                    'relationships' => [
                        'restaurants' => [
                            'data' => [
                                'type' => 'restaurants',
                                'id' => $restaurant->id,
                            ],
                        ],
                        'meals' => [
                            'data' => [
                                [
                                    'type' => 'meals',
                                    'id' => $meal->id,
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['regular']['access_token'],
            ]
        );
        
        // Check basic response structure
        $this->commonSingleVndCheck($response, 'orders');
    }

    /**
     * Test GET all
     *
     * @return void
     */
    public function testGetAll()
    {
        // Login
        $this->login('admin');

        $count_ori = Orders::count();

        // Create 1 more item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        Orders::create([
            'restaurant_id' => $restaurant->id,
            'status' => 1,
        ]);

        $response = $this->json(
            'GET',
            '/api/fooddelivery/orders',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonMultipleVndCheck($response, 'orders');

        // Check total number
        $count_new = count($response->json()['data']);
        $this->assertEquals($count_new, $count_ori + 1);
    }

    /**
     * Test GET
     *
     * @return void
     */
    public function testGet()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        Meals::create([
            'restaurant_id' => $restaurant->id,
            'name' => 'Pasta',
            'description' => 'Best in Italy',
            'price' => 5.75,
        ]);

        $meals = Meals::all();
        $data = [];
        foreach ($meals as $meal) {
            $data[$meal->id] = [
                'ordered_at' => now()->timestamp,
                'price' => $meal->price,
                'quantity' => 2,
            ];
        }
        
        $order = Orders::create([
            'restaurant_id' => $restaurant->id,
            'status' => 1,
        ]);
        $order->meals()->attach($data);

        // This should return a Orders
        $response = $this->json(
            'GET',
            '/api/fooddelivery/orders/'.$order->id,
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'orders');
    }

    /**
     * Test PATCH
     * We skip this test because we have difficulties to switch the role.
     * Test done on Postman instead
     *
     * @return void
     */
    public function SKIPtestPatch()
    {
        // Login
        $this->login('owner');
        
        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        Meals::create([
            'restaurant_id' => $restaurant->id,
            'name' => 'Pasta',
            'description' => 'Best in Italy',
            'price' => 5.75,
        ]);

        $meals = Meals::all();
        $data = [];
        foreach ($meals as $meal) {
            $data[$meal->id] = [
                'ordered_at' => now()->timestamp,
                'price' => $meal->price,
                'quantity' => 2,
            ];
        }
        
        /**
         * NOTE: This does not work here because an owner cannot create an Order
         */

        $order = Orders::create([
            'restaurant_id' => $restaurant->id,
            'status' => 1,
        ]);
        $order->meals()->attach($data);

        // Patch
        $response = $this->json(
            'PATCH',
            '/api/fooddelivery/orders/'.$order->id,
            [
                'data' => [
                    'type' => 'orders',
                    'id' => $order->id,
                    'attributes' => [
                        'status' => 2,
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['owner']['access_token'],
            ]
        );
        
        // Check basic response structure
        $this->commonSingleVndCheck($response, 'orders');

        // Check some values
        $response->assertJson([
            'data' => [
                'type' => 'orders',
                'attributes' => [
                    'status' => 2,
                ],
            ],
        ]);
    }

    /**
     * Test DELETE (soft)
     *
     * @return void
     */
    public function testDelete()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        Meals::create([
            'restaurant_id' => $restaurant->id,
            'name' => 'Pasta',
            'description' => 'Best in Italy',
            'price' => 5.75,
        ]);

        $meals = Meals::all();
        $data = [];
        foreach ($meals as $meal) {
            $data[$meal->id] = [
                'ordered_at' => now()->timestamp,
                'price' => $meal->price,
                'quantity' => 2,
            ];
        }
        
        $order = Orders::create([
            'restaurant_id' => $restaurant->id,
            'status' => 1,
        ]);
        $order->meals()->attach($data);

        // Delete
        $response = $this->json(
            'DELETE',
            '/api/fooddelivery/orders/'.$order->id,
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check Deletion status
        $response->assertStatus(204);

        // Get the deleted item
        $order = Orders::withTrashed()->find($order->id);

        // Check that the object exists
        $this->assertNotNull($order);

        // Check that it is soft deleted
        $this->assertNotNull($order->deleted_at);
    }

    /**
     * Test DELETE (hard)
     *
     * @return void
     */
    public function testDeleteForce()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        Meals::create([
            'restaurant_id' => $restaurant->id,
            'name' => 'Pasta',
            'description' => 'Best in Italy',
            'price' => 5.75,
        ]);

        $meals = Meals::all();
        $data = [];
        foreach ($meals as $meal) {
            $data[$meal->id] = [
                'ordered_at' => now()->timestamp,
                'price' => $meal->price,
                'quantity' => 2,
            ];
        }
        
        $order = Orders::create([
            'restaurant_id' => $restaurant->id,
            'status' => 1,
        ]);
        $order->meals()->attach($data);

        // Force to delete
        $response = $this->json(
            'PUT',
            '/api/fooddelivery/orders/'.$order->id.'/force_delete',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Item deleted
        $response->assertStatus(204);

        // Try to get the deleted item
        $order = Orders::withTrashed()->find($order->id);

        // Check that the object is deleted
        $this->assertNull($order);
    }

    /**
     * Test RESTORE
     *
     * @return void
     */
    public function testRestore()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        Meals::create([
            'restaurant_id' => $restaurant->id,
            'name' => 'Pasta',
            'description' => 'Best in Italy',
            'price' => 5.75,
        ]);

        $meals = Meals::all();
        $data = [];
        foreach ($meals as $meal) {
            $data[$meal->id] = [
                'ordered_at' => now()->timestamp,
                'price' => $meal->price,
                'quantity' => 2,
            ];
        }
        
        $order = Orders::create([
            'restaurant_id' => $restaurant->id,
            'status' => 1,
        ]);
        $order->meals()->attach($data);

        // Delete
        $order->delete();

        // Restore
        $response = $this->json(
            'PUT',
            '/api/fooddelivery/orders/'.$order->id.'/restore',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'orders');

        // Check Read status
        $response->assertStatus(200);

        // Check some values
        $response->assertJson([
            'data' => [
                'type' => 'orders',
                'id' =>  $order->id,
            ],
        ]);
    }

    /**
     * Test PATCH Fail (wrong role)
     *
     * @return void
     */
    public function testPatchFailRole()
    {
        // Login
        $this->login('regular');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        Meals::create([
            'restaurant_id' => $restaurant->id,
            'name' => 'Pasta',
            'description' => 'Best in Italy',
            'price' => 5.75,
        ]);

        $meals = Meals::all();
        $data = [];
        foreach ($meals as $meal) {
            $data[$meal->id] = [
                'ordered_at' => now()->timestamp,
                'price' => $meal->price,
                'quantity' => 2,
            ];
        }
        
        $order = Orders::create([
            'restaurant_id' => $restaurant->id,
            'status' => 1,
        ]);
        $order->meals()->attach($data);

        // Patch
        $response = $this->json(
            'PATCH',
            '/api/fooddelivery/orders/'.$order->id,
            [
                'data' => [
                    'type' => 'orders',
                    'id' => $order->id,
                    'attributes' => [
                        'status' => 2,
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['regular']['access_token'],
            ]
        );

        // Check that an expection is sent
        $this->assertTrue(isset($response->exception));
    }

    /**
     * Test PATCH Fail (wrong next status)
     *
     * @return void
     */
    public function testPatchFailStatus()
    {
        // Login
        $this->login('owner');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        Meals::create([
            'restaurant_id' => $restaurant->id,
            'name' => 'Pasta',
            'description' => 'Best in Italy',
            'price' => 5.75,
        ]);

        $meals = Meals::all();
        $data = [];
        foreach ($meals as $meal) {
            $data[$meal->id] = [
                'ordered_at' => now()->timestamp,
                'price' => $meal->price,
                'quantity' => 2,
            ];
        }
        
        $order = Orders::create([
            'restaurant_id' => $restaurant->id,
            'status' => 1,
        ]);
        $order->meals()->attach($data);

        // Patch
        $response = $this->json(
            'PATCH',
            '/api/fooddelivery/orders/'.$order->id,
            [
                'data' => [
                    'type' => 'orders',
                    'id' => $order->id,
                    'attributes' => [
                        'status' => 3,
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['owner']['access_token'],
            ]
        );

        // Check that an expection is sent
        $this->assertTrue(isset($response->exception));
    }
}
