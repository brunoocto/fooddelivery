<?php

namespace Tests\Feature\Models;

use Tests\TestCase;
use App\Models\Restaurants;

class RestaurantsTest extends TestCase
{
    /**
     * Test POST
     *
     * @return void
     */
    public function testPost()
    {
        // Login
        $this->login('admin');

        // Success
        $response = $this->json(
            'POST',
            '/api/fooddelivery/restaurants',
            [
                'data' => [
                    'type' => 'restaurants',
                    'attributes' => [
                        'name' => 'Resto',
                        'description' => 'Italian Food',
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'restaurants');
    }

    /**
     * Test GET all
     *
     * @return void
     */
    public function testGetAll()
    {
        // Login
        $this->login('admin');

        $count_ori = Restaurants::count();

        // Create 1 more item
        Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        $response = $this->json(
            'GET',
            '/api/fooddelivery/restaurants',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonMultipleVndCheck($response, 'restaurants');

        // Check total number
        $count_new = count($response->json()['data']);
        $this->assertEquals($count_new, $count_ori + 1);
    }

    /**
     * Test GET
     *
     * @return void
     */
    public function testGet()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        // This should return a Restaurants
        $response = $this->json(
            'GET',
            '/api/fooddelivery/restaurants/'.$restaurant->id,
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'restaurants');
    }

    /**
     * Test PATCH
     *
     * @return void
     */
    public function testPatch()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        // Patch
        $response = $this->json(
            'PATCH',
            '/api/fooddelivery/restaurants/'.$restaurant->id,
            [
                'data' => [
                    'type' => 'restaurants',
                    'id' => $restaurant->id,
                    'attributes' => [
                        'name' => 'Bistro',
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'restaurants');

        // Check some values
        $response->assertJson([
            'data' => [
                'type' => 'restaurants',
                'attributes' => [
                    'name' => 'Bistro',
                ],
            ],
        ]);
    }

    /**
     * Test DELETE (soft)
     *
     * @return void
     */
    public function testDelete()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        // Delete
        $response = $this->json(
            'DELETE',
            '/api/fooddelivery/restaurants/'.$restaurant->id,
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check Deletion status
        $response->assertStatus(204);

        // Get the deleted item
        $restaurant = Restaurants::withTrashed()->find($restaurant->id);

        // Check that the object exists
        $this->assertNotNull($restaurant);

        // Check that it is soft deleted
        $this->assertNotNull($restaurant->deleted_at);
    }

    /**
     * Test DELETE (hard)
     *
     * @return void
     */
    public function testDeleteForce()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        // Force to delete
        $response = $this->json(
            'PUT',
            '/api/fooddelivery/restaurants/'.$restaurant->id.'/force_delete',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Item deleted
        $response->assertStatus(204);

        // Try to get the deleted item
        $restaurant = Restaurants::withTrashed()->find($restaurant->id);

        // Check that the object is deleted
        $this->assertNull($restaurant);
    }

    /**
     * Test RESTORE
     *
     * @return void
     */
    public function testRestore()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        // Delete
        $restaurant->delete();

        // Restore
        $response = $this->json(
            'PUT',
            '/api/fooddelivery/restaurants/'.$restaurant->id.'/restore',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'restaurants');

        // Check Read status
        $response->assertStatus(200);

        // Check some values
        $response->assertJson([
            'data' => [
                'type' => 'restaurants',
                'id' =>  $restaurant->id,
            ],
        ]);
    }
}
