<?php

namespace Tests\Feature\Models;

use Tests\TestCase;
use App\Models\Meals;
use App\Models\Restaurants;
use App\Models\Orders;

class MealsTest extends TestCase
{
    /**
     * Test POST
     *
     * @return void
     */
    public function testPost()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        // Success
        $response = $this->json(
            'POST',
            '/api/fooddelivery/meals',
            [
                'data' => [
                    'type' => 'meals',
                    'attributes' => [
                        'name' => 'Pasta',
                        'description' => 'Best in Italy',
                        'price' => 5.75,
                    ],
                    'relationships' => [
                        'restaurants' => [
                            'data' => [
                                'type' => 'restaurants',
                                'id' => $restaurant->id,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'meals');
    }

    /**
     * Test GET all
     *
     * @return void
     */
    public function testGetAll()
    {
        // Login
        $this->login('admin');

        $count_ori = Meals::count();

        // Create 1 more item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        Meals::create([
            'restaurant_id' => $restaurant->id,
            'name' => 'Pasta',
            'description' => 'Best in Italy',
            'price' => 5.75,
        ]);

        $response = $this->json(
            'GET',
            '/api/fooddelivery/meals',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonMultipleVndCheck($response, 'meals');

        // Check total number
        $count_new = count($response->json()['data']);
        $this->assertEquals($count_new, $count_ori + 1);
    }

    /**
     * Test GET
     *
     * @return void
     */
    public function testGet()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        $meal = Meals::create([
            'restaurant_id' => $restaurant->id,
            'name' => 'Pasta',
            'description' => 'Best in Italy',
            'price' => 5.75,
        ]);

        // This should return a Meals
        $response = $this->json(
            'GET',
            '/api/fooddelivery/meals/'.$meal->id,
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'meals');
    }

    /**
     * Test PATCH
     *
     * @return void
     */
    public function testPatch()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        $meal = Meals::create([
            'restaurant_id' => $restaurant->id,
            'name' => 'Pasta',
            'description' => 'Best in Italy',
            'price' => 5.75,
        ]);

        // Patch
        $response = $this->json(
            'PATCH',
            '/api/fooddelivery/meals/'.$meal->id,
            [
                'data' => [
                    'type' => 'meals',
                    'id' => $meal->id,
                    'attributes' => [
                        'name' => 'patron',
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'meals');

        // Check some values
        $response->assertJson([
            'data' => [
                'type' => 'meals',
                'attributes' => [
                    'name' => 'patron',
                ],
            ],
        ]);
    }

    /**
     * Test DELETE (soft)
     *
     * @return void
     */
    public function testDelete()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        $meal = Meals::create([
            'restaurant_id' => $restaurant->id,
            'name' => 'Pasta',
            'description' => 'Best in Italy',
            'price' => 5.75,
        ]);

        // Delete
        $response = $this->json(
            'DELETE',
            '/api/fooddelivery/meals/'.$meal->id,
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check Deletion status
        $response->assertStatus(204);

        // Get the deleted item
        $meal = Meals::withTrashed()->find($meal->id);

        // Check that the object exists
        $this->assertNotNull($meal);

        // Check that it is soft deleted
        $this->assertNotNull($meal->deleted_at);
    }

    /**
     * Test DELETE (hard)
     *
     * @return void
     */
    public function testDeleteForce()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        $meal = Meals::create([
            'restaurant_id' => $restaurant->id,
            'name' => 'Pasta',
            'description' => 'Best in Italy',
            'price' => 5.75,
        ]);

        // Force to delete
        $response = $this->json(
            'PUT',
            '/api/fooddelivery/meals/'.$meal->id.'/force_delete',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Item deleted
        $response->assertStatus(204);

        // Try to get the deleted item
        $meal = Meals::withTrashed()->find($meal->id);

        // Check that the object is deleted
        $this->assertNull($meal);
    }

    /**
     * Test RESTORE
     *
     * @return void
     */
    public function testRestore()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $restaurant = Restaurants::create([
            'name' => 'Resto',
            'description' => 'Italian Food',
        ]);

        $meal = Meals::create([
            'restaurant_id' => $restaurant->id,
            'name' => 'Pasta',
            'description' => 'Best in Italy',
            'price' => 5.75,
        ]);

        // Delete
        $meal->delete();

        // Restore
        $response = $this->json(
            'PUT',
            '/api/fooddelivery/meals/'.$meal->id.'/restore',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'meals');

        // Check Read status
        $response->assertStatus(200);

        // Check some values
        $response->assertJson([
            'data' => [
                'type' => 'meals',
                'id' =>  $meal->id,
            ],
        ]);
    }
}
