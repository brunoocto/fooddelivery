<?php

namespace Tests\Feature\Models;

use Tests\TestCase;
use App\Models\Roles;
use App\Models\Permissions;

class PermissionsTest extends TestCase
{
    /**
     * Test POST
     *
     * @return void
     */
    public function testPost()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        // Success
        $response = $this->json(
            'POST',
            '/api/fooddelivery/permissions',
            [
                'data' => [
                    'type' => 'permissions',
                    'attributes' => [
                        'model' => 'meals',
                    ],
                    'relationships' => [
                        'roles' => [
                            'data' => [
                                'type' => 'roles',
                                'id' => $role->id,
                            ],
                        ],
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'permissions');
    }

    /**
     * Test GET all
     *
     * @return void
     */
    public function testGetAll()
    {
        // Login
        $this->login('admin');

        $count_ori = Permissions::count();

        // Create 1 more Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        Permissions::create([
            'role_id' => $role->id,
            'model' => 'meals',
        ]);

        $response = $this->json(
            'GET',
            '/api/fooddelivery/permissions',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonMultipleVndCheck($response, 'permissions');

        // Check total number
        $count_new = count($response->json()['data']);
        $this->assertEquals($count_new, $count_ori + 1);
    }

    /**
     * Test GET
     *
     * @return void
     */
    public function testGet()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        $permission = Permissions::create([
            'role_id' => $role->id,
            'model' => 'meals',
        ]);

        // This should return a Permissions
        $response = $this->json(
            'GET',
            '/api/fooddelivery/permissions/'.$permission->id,
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'permissions');
    }

    /**
     * Test PATCH
     *
     * @return void
     */
    public function testPatch()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        $permission = Permissions::create([
            'role_id' => $role->id,
            'model' => 'meals',
        ]);

        // Patch
        $response = $this->json(
            'PATCH',
            '/api/fooddelivery/permissions/'.$permission->id,
            [
                'data' => [
                    'type' => 'permissions',
                    'id' => $permission->id,
                    'attributes' => [
                        'model' => 'restaurants',
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'permissions');

        // Check some values
        $response->assertJson([
            'data' => [
                'type' => 'permissions',
                'attributes' => [
                    'model' => 'restaurants',
                ],
            ],
        ]);
    }

    /**
     * Test PATCH Fail
     *
     * @return void
     */
    public function testPatchFail()
    {
        // Login
        $this->login('owner');

        // Create a new Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        $permission = Permissions::create([
            'role_id' => $role->id,
            'model' => 'meals',
        ]);

        // Patch
        $response = $this->json(
            'PATCH',
            '/api/fooddelivery/permissions/'.$permission->id,
            [
                'data' => [
                    'type' => 'permissions',
                    'id' => $permission->id,
                    'attributes' => [
                        'model' => 'restaurants',
                    ],
                ],
            ],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['owner']['access_token'],
            ]
        );

        // Check that an expection is sent
        $this->assertTrue(isset($response->exception));
    }

    /**
     * Test DELETE (soft)
     *
     * @return void
     */
    public function testDelete()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        $permission = Permissions::create([
            'role_id' => $role->id,
            'model' => 'meals',
        ]);

        // Delete
        $response = $this->json(
            'DELETE',
            '/api/fooddelivery/permissions/'.$permission->id,
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check Deletion status
        $response->assertStatus(204);

        // Get the deleted item
        $permission = Permissions::withTrashed()->find($permission->id);

        // Check that the object exists
        $this->assertNotNull($permission);

        // Check that it is soft deleted
        $this->assertNotNull($permission->deleted_at);
    }

    /**
     * Test DELETE (hard)
     *
     * @return void
     */
    public function testDeleteForce()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        $permission = Permissions::create([
            'role_id' => $role->id,
            'model' => 'meals',
        ]);

        // Force to delete
        $response = $this->json(
            'PUT',
            '/api/fooddelivery/permissions/'.$permission->id.'/force_delete',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Item deleted
        $response->assertStatus(204);

        // Try to get the deleted item
        $permission = Permissions::withTrashed()->find($permission->id);

        // Check that the object is deleted
        $this->assertNull($permission);
    }

    /**
     * Test RESTORE
     *
     * @return void
     */
    public function testRestore()
    {
        // Login
        $this->login('admin');

        // Create a new Item
        $role = Roles::create([
            'name' => 'boss',
        ]);

        $permission = Permissions::create([
            'role_id' => $role->id,
            'model' => 'meals',
        ]);

        // Delete
        $permission->delete();

        // Restore
        $response = $this->json(
            'PUT',
            '/api/fooddelivery/permissions/'.$permission->id.'/restore',
            [],
            [
                'Content-Type' => 'application/vnd.api+json',
                'Accept' => 'application/vnd.api+json',
                'Api-Key' => env('API_KEY'),
                'Authorization' => 'Bearer '.$this->data['admin']['access_token'],
            ]
        );

        // Check basic response structure
        $this->commonSingleVndCheck($response, 'permissions');

        // Check Read status
        $response->assertStatus(200);

        // Check some values
        $response->assertJson([
            'data' => [
                'type' => 'permissions',
                'id' =>  $permission->id,
            ],
        ]);
    }
}
