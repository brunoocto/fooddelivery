<?php

namespace Tests\Feature;

use Tests\TestCase;

class DummyTest extends TestCase
{
    /**
     * Dummy Get
     *
     * @return void
     */
    public function testDummyGet()
    {
        // Success
        $response = $this->json(
            'GET',
            '/api/test',
            [],
            [
                'Accept' => 'application/json',
                'Api-Key' => env('API_KEY'),
            ]
        );

        // Check Content
        $response->assertSeeText('getTest');

        // Check status
        $response->assertStatus(200);
    }

    /**
     * Dummy Post
     *
     * @return void
     */
    public function testDummyPost()
    {
        // Success
        $response = $this->json(
            'POST',
            '/api/test',
            [
                'test' => 'something'
            ],
            [
                'Accept' => 'application/json',
                'Api-Key' => env('API_KEY'),
            ]
        );

        // Check Content
        $response->assertSeeText('something');

        // Check status
        $response->assertStatus(200);
    }

    /**
     * Dummy Get (Fail)
     *
     * @return void
     */
    public function testDummyGetFail()
    {
        // Fail (Missing API KEY)
        $response = $this->json(
            'GET',
            '/api/test',
            [],
            [
                'Accept' => 'application/json',
            ]
        );

        // Check that an expection is sent
        $this->assertTrue(isset($response->exception));
    }

    /**
     * Dummy Get 404 (Fail)
     *
     * @return void
     */
    public function testDummy404Fail()
    {
        // Fail (Wrong route)
        $response = $this->json(
            'GET',
            '/noroute',
            [],
            [
                'Accept' => 'application/json',
                'Api-Key' => env('API_KEY'),
            ]
        );
        
        // Check status
        $response->assertStatus(302);
        // Check Content
        $response->assertSeeText('404');
    }

    /**
     * Dummy No Auth (Fail)
     *
     * @return void
     */
    public function testDummyAuthFail()
    {
        // Fail (No authentication)
        $response = $this->json(
            'GET',
            '/api/test/auth',
            [],
            [
                'Accept' => 'application/json',
                'Api-Key' => env('API_KEY'),
            ]
        );
        
        // Check status
        $response->assertStatus(401);
    }
}
