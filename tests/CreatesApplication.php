<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Tests\SQLiteTestingConnector;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        /**
         *  To enable the memory database not constantly refreshing, we overwrite the registrering process of ":memory:" to keep one single connection after application refresh call.
         * using ":shared-memory:" does not work as describe in the below link because of SQLiteBuilder@dropAllTables which works only with ":memory:".
         * https://qiita.com/crhg/items/c53e9381f6c976f211c1
         */
        $app->singleton('db.connector.sqlite', SQLiteTestingConnector::class);

        return $app;
    }
}
