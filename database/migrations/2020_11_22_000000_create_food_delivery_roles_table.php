<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodDeliveryRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('fooddelivery')->create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps(3);
            $table->dateTime('deleted_at', 3)->nullable();

            $table->string('name', 190)->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::connection('fooddelivery')->dropIfExists('roles');
    }
}
