<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodDeliveryOrderXMealTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('fooddelivery')->create('order_x_meal', function (Blueprint $table) {
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('meal_id');
            $table->unique(['order_id', 'meal_id']);

            // We store a timestamp here since we cannot cast a pivot value
            $table->unsignedInteger('ordered_at');
            $table->float('price', 8, 2);
            $table->unsignedSmallInteger('quantity')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::connection('fooddelivery')->dropIfExists('order_x_meal');
    }
}
