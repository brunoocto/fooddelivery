<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodDeliveryPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('fooddelivery')->create('permissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps(3);
            $table->dateTime('deleted_at', 3)->nullable();

            $table->string('model');
            $table->unsignedBigInteger('role_id');
            $table->boolean('create')->default(false);
            $table->boolean('read')->default(true);
            $table->boolean('update')->default(false);
            $table->boolean('delete')->default(false);

            $table->unique(['model', 'role_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::connection('fooddelivery')->dropIfExists('permissions');
    }
}
