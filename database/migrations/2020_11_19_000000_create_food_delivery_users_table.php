<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodDeliveryUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('fooddelivery')->create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps(3);
            $table->string('email', 190)->unique();
            $table->string('name');
            $table->string('password');
            $table->rememberToken();
            $table->timestamp('email_verified_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // I do not allow "drop" feature to avoid any unwanted table deletion (which can happen while a rollback operation)
        // Schema::connection('fooddelivery')->dropIfExists('users');
    }
}
