<?php

use Illuminate\Database\Seeder;
use Laravel\Passport\Token as OauthAccessTokens;

class OauthAccessTokensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (OauthAccessTokens::count() == 0) {
            
            // Admin Token
            // $access_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiMmUzMWRkNzgzMjczN2VkZjFlZDhjM2M1YTA3MjUyODgxYzA4MzZjOTkxNzRkMjIzNmI3MTY3MzU2ZWFlMDRmNjFlMzRjODY2MTNkY2FmYjkiLCJpYXQiOjE2MDYxMTE5MzIsIm5iZiI6MTYwNjExMTkzMiwiZXhwIjoxNjA4NzAzOTMxLCJzdWIiOiIxIiwic2NvcGVzIjpbImFkbWluIl19.gBtGWqSRsDSss3UldVRpqS7de2X00Wo4k5QZHSyw3nLJNaXlw_TQ4KPVoBm2dPFETR8uMA0aSqLFfnvPH61n3OmIWfGA-uCXmPQv8QGCuuwMk0jUG3Nx9TImp36VtCt2zGZJCZrd83pbLn0Xi-Zukjx5fM9PTTWx3iEIkO0dADHPda3i7QHyRtO6WV0JKf6HNdbhXcghFhAguGaWg_e86birwWLxJtaeTYxJqSX-Phe5Kc5N2QX5FzPSi_DCErIvnzDBBhuNsUUw17x2EugdTIPIAFlTeqm40CUc2drlX076mo5xLptvd9f4f9mcRqitgcR1PdDEW8xdPq54C0mer8siBTaK5zZJ2F4WzBrSz4rUVpUXYIQb-oJbEQkhDA4Q-V6Nmo9RdDFcRakSPdpc4logfJEPX8nfbH_cE9pcebhaaq_lPn1PMwmeWTSQzmQtZevZs2I3bD5yXqJgOZN3G4rtyLlmp0N4RW88OlTJKBXzVGjQc3M-O-Jk8NAsG409xwPynn7TVe0RIO7awAFUPqtPaX_FKbVf6_9TQEpElz1I5pUu-ljW2aGL6UUWEaYsq8wRs64H2lnIHIUJlPhXzVF6ZjOWCGkZMHk38RC4S_GuxMaAGvRsOKZfpvwI8-nbrb_iMp5OIPIscSHdVemcwlMKIi451Y7yqAIjRI0BR6s';
            $oauth_access_token = OauthAccessTokens::create([
                'id' => '2e31dd7832737edf1ed8c3c5a07252881c0836c99174d2236b7167356eae04f61e34c86613dcafb9',
                'user_id' => '1',
                'client_id' => '2',
                'scopes' => ['admin'],
                'revoked' => false,
                'expires_at' => null,
            ]);

            // Owner Token
            // $access_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiZTE1ZWNmZTg0NGU2NWIzNmFiNjE2YzRhMWY5NGExY2M0MWE4ZmIwMWY1OGJhZmFmN2Q1YzFkMmZlY2I1NWUwZjQxOWRmMDhjY2VlYTE0NmUiLCJpYXQiOjE2MDYxMTIyOTEsIm5iZiI6MTYwNjExMjI5MSwiZXhwIjoxNjA4NzA0MjkxLCJzdWIiOiIyIiwic2NvcGVzIjpbIm93bmVyIl19.pdGQ8tJ6BFzx-DiD2knxxM3a5xKRt1NDc9mZXROPvY8CLvDlm3P-cWjOkLkw-08FgHGWIgqwWG-gMebZuHPqdSjodyz6ADo5PE69TbMkpc8mHDltX7rTfq44VYsK8DAP9F84fIbHi6DppjiRQe6TFReYRsjPgwk24QFOZRUrFE440IZHeYjPXSuxxcvBglU8CXIaXbvoaQo4C8IrYJykbDrxgCc_kK1d_cRftJCb3CaxVoAwMtul2zcoltGw0pex4CePG9k9x1vmCuhWC5mwNmh5rHV7lDvtDdVp8o9sit1TTQcsGwUN1_Dcnhdz6NQmI3RhocPYu9eiHsMjz5P43O-ejt1ZeqzqCQz0Qb9_4f3x0oDmkyLw2fEYfexD9zfKJNynv1JbjwgcIvyy3gaoI572-lCPs7LtiisY-feHDXnWs-FnHoobkNWLRqxK9bYmkB02pDIWnYp4W2kf5BxrDyaSEhpx0OdLbIpDb9KThjLP1u36q5o4x90ZEagIIfqPoCCymt08knYp--PTIzNIDXzV8Bquo_vURCfWZAxGa_MM3TsfyL-CoqbKAXY6HG7MrN4X6SAoCBHt5pmAWiLYKLqqq8wNYFd5EDeQi1lkK-vkPz3YvqJ1ghMNdMHAVyjwUriAWZ5JWXA35w1bqSx1eY_nCi4cCVCJS6tvDMducfI';
            $oauth_access_token = OauthAccessTokens::create([
                'id' => 'e15ecfe844e65b36ab616c4a1f94a1cc41a8fb01f58bafaf7d5c1d2fecb55e0f419df08cceea146e',
                'user_id' => '2',
                'client_id' => '2',
                'scopes' => ['owner'],
                'revoked' => false,
                'expires_at' => null,
            ]);

            // Regular Token
            // $access_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIyIiwianRpIjoiNDhlMDQ0MzkxMTk1NzhhMDE1YzE2NDFiNzE4NzE3ODJlOTczNzkzN2RmYWQyYjQwOWI5NGExZmIzOTY0NjRiNTkxMDhhNjE1MGI4MTNjODMiLCJpYXQiOjE2MDYxMTIzNjgsIm5iZiI6MTYwNjExMjM2OCwiZXhwIjoxNjA4NzA0MzY4LCJzdWIiOiIyIiwic2NvcGVzIjpbInJlZ3VsYXIiXX0.lRgVoPB2-oyb0bqGLZ9F0hhpMan4n0P4dJFC2z6wao509zCIZvW4QcUd4EsAQsksq6KqwfgFa4mkLsbb9nt6EtutD2yqWMUxe22iduQ3DqEmlCymj6lBxdSbhOR48jWlWR5FopOtQZvJ7sg-qkZbbCDp1oPkLNQIcYPhVC5s41-q6ngaHS8Ih3UdsEAOATI3anStyJTcbg2X3Beb1xn31lZNWcgmPbLx1famnpsDmmJ7uKk01G-xzTTcSFUQY4fJUOSDvZH6VUn7mE3GaYz71J3aH-Zxi56UcsLNCGOj_S4Xmhs5SwHocFXjIIvglo0BZIGX444xfwwG-l2sb2BJdmH-vs0eGMEvqP4QfZhU9QgGSyqWv7FPhOPjMUEr8_cXbL99t-dfl3bgLXCTIWt_xnu51dEAMv3Y5lZrvykjEKK9btxpzdaKOhkkv90RYsUvdaYh1o2RsFVe90t0IYQ0bM3whRWksTJ9-ORczYVJCiIoBK8lTJG8WUEF9nN_wDbpCh0VDjmPae6CZTYFRyxJtkLNBo8qSwfhmBz8jEY_ITIUhneFtCyAb-tJGHMeJT_Ads8Ey3E74HOlY_UgbPchfBW4sGhtMKOc4wbwjXMZxM4c0s68R4yH74M2zxpEk0k2HketCGvwl1od74kEURIQ7-5Vng8mX01CgDhtM2kMXFQ';
            $oauth_access_token = OauthAccessTokens::create([
                'id' => '48e04439119578a015c1641b71871782e9737937dfad2b409b94a1fb396464b59108a6150b813c83',
                'user_id' => '2',
                'client_id' => '2',
                'scopes' => ['regular'],
                'revoked' => false,
                'expires_at' => null,
            ]);
        }
    }
}
