<?php

use Illuminate\Database\Seeder;
use Laravel\Passport\RefreshToken as OauthRefreshTokens;

class OauthRefreshTokensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (OauthRefreshTokens::count() == 0) {
            
            // Admin Token
            // $refresh_token = 'def50200217788f23d26cc7f64c86ec999bab24144951bb7c8151ab93447fe4ca9d0fa1adb64a873a841fe313c0aa3203bd00875c8fc32c93d6fc1d626a6c6016bb6719a240be04adefa02769ec8000d0b200ef3f068bc55db5f05a8ff03ead7b65aed6c037cfbf0bb9d394813ef000b6cb3f7c309e2cca9404ece55a5d52117c923ed3c17c8e9a03ede98c6d6d43d64478e9c3d33c4b461bc79a2f6cbbb69e36172daeee068e5715db2e143c204e4a4f1cf9a232ed05b89da70abd5fda4c2a32efca3ac34c9ae55f1025f365ca75924f5171ec92f736705f03a0c16067f6a87d615686275839fda380dc69197f1022b0b377e09b911a9e08f937f430906601e876210e48ea586c7e7df1ff7e645db516a2a386d9f6732ef769756c57b877e6e2eb438cfbb111d4acf31fe4a245f3ad0368f519ee8c6cf4a87360a5b1c2b1af58504bc0b9db02352d2e990bd471b89113d78b71d53c098f263f74bb9fdbee2bb78b757aa22125abb417d';
            $oauth_refresh_token = OauthRefreshTokens::create([
                'id' => '3a2c38f69c48cb4ce62f2147d126840e511a9a3f1ce776f67a7221278ebd22d1f7399be5c16b0a6e',
                'access_token_id' => '2e31dd7832737edf1ed8c3c5a07252881c0836c99174d2236b7167356eae04f61e34c86613dcafb9',
                'revoked' => false,
                'expires_at' => null,
            ]);

            // Owner Token
            // $refresh_token = 'def502008a8df1f9d2b1cdf2a6cf935a00656b4b56e40d81d61588bb548354aa99d6383ca0ad4b34f10e178c68930c06cb26b5ce74f129a2c7cfe2528551364cdbae34ea62b1ca801ebcb23f39b6f1457c30707c649f37c4bc8b4de41d188880039084ae9b656c5efccd50fdec84904156d5b85201426386f700166d85860408c9e8d00b58dafb407afca22d891a7f07bdf6c7aa56051347d08603bd326ff332776db0017a272ede508a25798f34c3d44ba273d25aa64ada1d37c20496565dad0cd9757fc11fcb20ab9fd1980d1a21e866b0de875661d1bf75e6be9aedb7f339b0dd94bf78fa30a29bbbf2c763b47fd5fe6a916a5c0c33540bfab1f7342ba8e740c37bbfbb9213df76e6007eb2d697206e819f9f5aa1e29b5f2af4559c08966bec6d60dffb0b665229db3ab54bf3431a435da868dd508904b8c580c5118c03e12fa4dc0302f0ba3e84674d0e1b853d0abfc3bf6aa3e5d22f8cbdb1b81e6c94312f88174d3ad7c6a169bc';
            $oauth_refresh_token = OauthRefreshTokens::create([
                'id' => '09fbeb8d585d5379122a884900e2de0f8dc4ca8408589037bec2b7a5478a61d07b04ca7edaa5df12',
                'access_token_id' => 'e15ecfe844e65b36ab616c4a1f94a1cc41a8fb01f58bafaf7d5c1d2fecb55e0f419df08cceea146e',
                'revoked' => false,
                'expires_at' => null,
            ]);

            // Regular Token
            // $refresh_token = 'def5020043bbdcf52997f603439ce2fd33267d4c4cb1ee3402fe898cf797e2d141cdc0008e477f3bcae44fe35c1fc1bbc5426c3153adacd42ae89618b5a672ad05c9eae6ba82ec48dd1486879055a827dc486050128de502d8fac6ed7646888911a56c036cfdd4f6e74053d763928cd3f6a4bb25c1a6514e8715c178834eb4e02627b94235a1f2fbeadcc355fafff9118999099ea551cc82bdc05cee9731a6ade7b9831826a013b4ff5367afa8d7302c8ba395bc1b294d838efc992fad829ed1b9a72cae194bd2a0d08eeba7acf93deb8ce7a35868befbe74b9b59b19855939d9cf843722d0b73a8b38b00edb57fe0f48353ce7f52464f8f70eac010ddd4f95877f20e4ac7913f377114a049198d579f599b311064d484c0d98611ae159d1c5361f5daf8f5cf541b885c0ef98dd301a925b0411d634531c05420752e29c5bc00465e4d8d4cf518c75483d40025c483e675135d36751a8576e130973b245cc8504c71866c0a1b4d87af7fcdda';
            $oauth_refresh_token = OauthRefreshTokens::create([
                'id' => '46b0d0a6c9fa23d43f2eb44945088a02c21f2df24e46d5244e8c7142514da6ee838c6e157f0cb257',
                'access_token_id' => '48e04439119578a015c1641b71871782e9737937dfad2b409b94a1fb396464b59108a6150b813c83',
                'revoked' => false,
                'expires_at' => null,
            ]);
        }
    }
}
