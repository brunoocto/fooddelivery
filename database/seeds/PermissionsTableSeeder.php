<?php

use Illuminate\Database\Seeder;
use App\Models\Roles;
use App\Models\Permissions;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Permissions::count() == 0) {
            /**
             * Add Permission for Owner
             * Only need for "orders" because all other tables can use Vmodel default permission
             */

            // Get the Onwer role
            $role = Roles::Where('name', 'owner')->first(['id']);

            $permission = Permissions::create([
                'role_id' => $role->id,
                'model' => 'orders',
                'create' => false,
                'read' => true,
                'update' => true,
                'delete' => false,
            ]);
            
            /**
             * Add Permission for Regular user
             * Only Users is using Vmodel default permission
             */

            // Get the Regular user role
            $role = Roles::Where('name', 'regular')->first(['id']);

            $permission = Permissions::create([
                'role_id' => $role->id,
                'model' => 'restaurants',
                'create' => false,
                'read' => true,
                'update' => false,
                'delete' => false,
            ]);

            $permission = Permissions::create([
                'role_id' => $role->id,
                'model' => 'meals',
                'create' => false,
                'read' => true,
                'update' => false,
                'delete' => false,
            ]);

            $permission = Permissions::create([
                'role_id' => $role->id,
                'model' => 'orders',
                'create' => true,
                'read' => true,
                'update' => true,
                'delete' => false,
            ]);
        }
    }
}
