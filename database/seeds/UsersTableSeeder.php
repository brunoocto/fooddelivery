<?php

use Illuminate\Database\Seeder;
use App\Models\Users;
use App\Models\Roles;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Users::count() == 0) {
            // Get all Roles
            $roles = Roles::all();
            // Create an Admin user with the whole app access
            $user = Users::create([
                'name' => 'Bruno Admin',
                'email' => 'admin@lincko.cafe',
                'password' => Hash::make('q1w2e3r4t5y6'),
            ]);
            $user->roles()->saveMany($roles);

            // Get Onwer and Regular Roles (Default User registration)
            $roles = Roles::whereIn('name', ['owner', 'regular'])->get();
            // Create an Owner user 1
            $user = Users::create([
                'name' => 'Bruno Owner',
                'email' => 'owner@lincko.cafe',
                'password' => Hash::make('q1w2e3r4t5y6'),
            ]);
            $user->roles()->saveMany($roles);
            // Create an Owner user 2
            $user = Users::create([
                'name' => 'Another Owner',
                'email' => 'another.owner@lincko.cafe',
                'password' => Hash::make('q1w2e3r4t5y6'),
            ]);
            $user->roles()->saveMany($roles);
            // A Regular User
            $user = Users::create([
                'name' => 'Bruno Regular',
                'email' => 'regular@lincko.cafe',
                'password' => Hash::make('q1w2e3r4t5y6'),
            ]);
            $user->roles()->saveMany($roles);
            // Anoher Regular User
            $user = Users::create([
                'name' => 'Bruno Regular',
                'email' => 'user@lincko.cafe',
                'password' => Hash::make('q1w2e3r4t5y6'),
            ]);
            $user->roles()->saveMany($roles);
        }
    }
}
