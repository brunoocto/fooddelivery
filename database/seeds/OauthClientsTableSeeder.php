<?php

use Illuminate\Database\Seeder;

use Laravel\Passport\Client as OauthClients;

class OauthClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (OauthClients::count() == 0) {
            
            // Personal Access Client
            $oauth_client = OauthClients::create([
                'name' => 'Food Delivery Personal Access Client',
                'secret' => 'sA9O7yjVJkRc2pERi2lLQb5lVOAqskJZzoHeXPYe',
                'redirect' => 'http://localhost',
                'personal_access_client' => true,
                'password_client' => false,
                'revoked' => false,
            ]);

            // Password Grant Client
            $oauth_client = OauthClients::create([
                'name' => 'Food Delivery Password Grant Client',
                'secret' => '3Qi9dmiw1UtExJSoCIT478atGG5TfRijZzA4BvXc',
                'redirect' => 'http://localhost',
                'personal_access_client' => false,
                'password_client' => true,
                'revoked' => false,
            ]);
        }
    }
}
