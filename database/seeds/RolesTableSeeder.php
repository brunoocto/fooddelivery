<?php

use Illuminate\Database\Seeder;
use App\Models\Roles;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Roles::count() == 0) {
            $role = Roles::create([
                'name' => 'admin',
            ]);

            $role = Roles::create([
                'name' => 'owner',
            ]);

            $role = Roles::create([
                'name' => 'regular',
            ]);
        }
    }
}
