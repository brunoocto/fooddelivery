<?php

use Illuminate\Support\Str;

// For sqlite database, we add database_path
$sqlite_db = env('DB_DATABASE');
if (env('APP_ENV') == 'testing') {
    $sqlite_db = ':memory:';
} else {
    $sqlite_db = database_path(env('DB_DATABASE'));
}

// For sqlite database, we add database_path
$fooddelivery_driver = !is_null(env('FOODDELIVERY_DB_DRIVER')) ? env('FOODDELIVERY_DB_DRIVER') : 'mysql';
$fooddelivery_db = env('FOODDELIVERY_DB_DATABASE');
if (env('APP_ENV') == 'testing') {
    $fooddelivery_driver = 'sqlite';
    $fooddelivery_db = ':memory:';
} elseif ($fooddelivery_driver == 'sqlite') {
    $fooddelivery_db = database_path(env('FOODDELIVERY_DB_DATABASE'));
}


return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => !is_null($sqlite_db) ? $sqlite_db : env('DB_DATABASE', 'forge'),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        'fooddelivery' => [
            'driver' => $fooddelivery_driver,
            'url' => !is_null(env('FOODDELIVERY_DATABASE_URL')) ? env('FOODDELIVERY_DATABASE_URL') : env('DATABASE_URL'),
            'host' => !is_null(env('FOODDELIVERY_DB_HOST')) ? env('FOODDELIVERY_DB_HOST') : env('DB_HOST', '127.0.0.1'),
            'port' => !is_null(env('FOODDELIVERY_DB_PORT')) ? env('FOODDELIVERY_DB_PORT') : env('DB_PORT', '3306'),
            'database' => !is_null($fooddelivery_db) ? $fooddelivery_db : env('DB_DATABASE', 'forge'),
            'username' => !is_null(env('FOODDELIVERY_DB_USERNAME')) ? env('FOODDELIVERY_DB_USERNAME') : env('DB_USERNAME', 'forge'),
            'password' => !is_null(env('FOODDELIVERY_DB_PASSWORD')) ? env('FOODDELIVERY_DB_PASSWORD') : env('DB_PASSWORD', ''),
            'unix_socket' => !is_null(env('FOODDELIVERY_DB_SOCKET')) ? env('FOODDELIVERY_DB_SOCKET') : env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => 'fooddelivery_',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => !is_null(env('FOODDELIVERY_MYSQL_ATTR_SSL_CA')) ? env('FOODDELIVERY_MYSQL_ATTR_SSL_CA') : env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
            'foreign_key_constraints' => !is_null(env('FOODDELIVERY_DB_FOREIGN_KEYS')) ? env('FOODDELIVERY_DB_FOREIGN_KEYS') : env('DB_FOREIGN_KEYS', false),
        ],

        'mysql' => [
            'driver' => 'mysql',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '1433'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => env('REDIS_CLIENT', 'phpredis'),

        'options' => [
            'cluster' => env('REDIS_CLUSTER', 'redis'),
            'prefix' => env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_database_'),
        ],

        'default' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_DB', '0'),
        ],

        'cache' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_CACHE_DB', '1'),
        ],

    ],

];
