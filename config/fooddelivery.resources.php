<?php

/**
 * List all models to register as VND resource
 */
return [
    'users' => \App\Models\Users::class,
    'roles' => \App\Models\Roles::class,
    'permissions' => \App\Models\Permissions::class,
    'restaurants' => \App\Models\Restaurants::class,
    'meals' => \App\Models\Meals::class,
    'orders' => \App\Models\Orders::class,
];
